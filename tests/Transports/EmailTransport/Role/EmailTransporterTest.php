<?php

namespace ServiceCore\Notification\Tests\Transports\EmailTransport\Role;

use Laminas\Mail\Transport\TransportInterface;
use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Notification\Transports\EmailTransport\Exception\FromDetailsRequiredException;
use ServiceCore\Notification\Transports\EmailTransport\Exception\ScriptsPathRequiredException;
use ServiceCore\Notification\Transports\EmailTransport\Role\EmailTransporter;
use ServiceCore\Notification\Transports\EmailTransport\RoleData\EmailTransporterData;

class EmailTransporterTest extends Test
{
    public function testTransport(): void
    {
        $transporter = new EmailTransporter($this->getNotification(), $this->getTransporter(), 'subaccount');

        self::assertTrue($transporter->transport([
            'scriptsPath' => 'foo',
            'layoutsPath' => 'bar',
            'fromAddress' => 'test@test.com',
        ]));
    }

    public function testTransportThrowsExceptionsWithInvalidOptions(): void
    {
        $transporter = new EmailTransporter($this->getNotification(), $this->getTransporter(), 'subaccount');

        $this->expectException(ScriptsPathRequiredException::class);

        $transporter->transport([]);

        $this->expectException(ScriptsPathRequiredException::class);

        $transporter->transport([
            'scriptsPath' => 'foo' // Need both scriptsPath and layoutsPath, so we will still get ScriptsPathRequired
        ]);

        $this->expectException(ScriptsPathRequiredException::class);

        $transporter->transport([
            'layoutsPath' => 'bar'
        ]);

        $this->expectException(FromDetailsRequiredException::class);

        $transporter->transport([
            'scriptsPath' => 'foo',
            'layoutsPath' => 'bar'
        ]);
    }

    private function getNotification(): EmailTransporterData
    {
        $builder = $this->getMockBuilder(EmailTransporterData::class);

        $builder->disableOriginalConstructor();

        $notification = $builder->getMock();

        $notification->method('getToAddress')
                     ->willReturn(['test@test.com']);

        return $notification;
    }

    private function getTransporter(): TransportInterface
    {
        /** @var TransportInterface $transporter */
        $transporter = $this->createMock(TransportInterface::class);

        return $transporter;
    }
}
