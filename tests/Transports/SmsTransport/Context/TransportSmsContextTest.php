<?php

namespace ServiceCore\Notification\Tests\Transports\SmsTransport\Context;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsTwilioAdapter;
use ServiceCore\Notification\Transports\SmsTransport\Context\TransportSmsContext;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsTransporterData;

class TransportSmsContextTest extends Test
{
    public function testTransport(): void
    {
        $toPhone      = '+13034323019';
        $fromPhone    = '+18443360611';
        $body         = 'iamtheverymodelofascientistsalarian';
        $optionsArray = [];

        $messageResult = new SmsMessageResult(123);

        $smsTransporterData = $this->createStub(SmsTransporterData::class);
        $smsTransporterData->method('getPhoneNumber')->willReturn($toPhone);
        $smsTransporterData->method('getShortBody')->willReturn($body);

        $smsAdapter = $this->createMock(SmsTwilioAdapter::class);
        $smsAdapter->expects($this->once())
            ->method('sendSms')
            ->with(
                $toPhone,
                $fromPhone,
                $body
            )
            ->willReturn($messageResult);

        $transportSmsContext = new TransportSmsContext(
            $smsTransporterData,
            $smsAdapter,
            $fromPhone
        );

        $result = $transportSmsContext->transport($optionsArray);

        $this->assertEquals($messageResult, $result);
    }
}
