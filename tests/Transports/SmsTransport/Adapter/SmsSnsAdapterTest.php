<?php

namespace ServiceCore\Notification\Tests\Transports\SmsTransport\Adapter;

use Aws\Command;
use Aws\Exception\AwsException;
use Aws\Result;
use Aws\Sns\SnsClient;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsSnsAdapter;

class SmsSnsAdapterTest extends Test
{
    public function testSendSms(): void
    {
        $body         = 'fakebody';
        $to           = '+18443360611';
        $id           = 'thisIsAnId';
        $testProperty = 'yep';

        $phoneNumberUtilStub = $this->createStub(PhoneNumberUtil::class);
        $phoneNumberUtilStub->method('parse')->willReturn(new PhoneNumber());
        $phoneNumberUtilStub->method('format')->willReturn($to);

        $snsClientMock = $this->createMock(SnsClient::class);
        $snsClientMock->expects($this->once())
            ->method('__call')
            ->with(
                'publish',
                [
                    [
                        'Message'     => $body,
                        'PhoneNumber' => $to
                    ]
                ]
            )
            ->willReturn(new Result(
                [
                    'MessageId'    => $id,
                    'testProperty' => $testProperty,
                ]
            ));

        $smsSnsAdapter = new SmsSnsAdapter(
            $snsClientMock,
            $phoneNumberUtilStub,
        );

        $result = $smsSnsAdapter->sendSms(
            $to,
            '',
            $body
        );

        $this->assertEquals($id, $result->sid);
        $this->assertEquals($testProperty, $result->testProperty);
    }

    public function testSendSmsHandlesAwsException(): void
    {
        $body         = 'fakebody';
        $to           = '+18443360611';
        $errorMessage = 'ohNoOurTableItsBroken';
        $exception    = new AwsException(
            $errorMessage,
            new Command('test'),
        );

        $phoneNumberUtilStub = $this->createStub(PhoneNumberUtil::class);
        $phoneNumberUtilStub->method('parse')->willReturn(new PhoneNumber());
        $phoneNumberUtilStub->method('format')->willReturn($to);

        $snsClientMock = $this->createMock(SnsClient::class);
        $snsClientMock->expects($this->once())
            ->method('__call')
            ->with(
                'publish',
                [
                    [
                        'Message'     => $body,
                        'PhoneNumber' => $to
                    ]
                ]
            )
            ->willThrowException($exception);

        $smsSnsAdapter = new SmsSnsAdapter(
            $snsClientMock,
            $phoneNumberUtilStub,
        );

        $result = $smsSnsAdapter->sendSms(
            $to,
            '',
            $body
        );

        $this->assertNull($result->sid);
        $this->assertEquals($errorMessage, $result->errorMessage);
        $this->assertEquals(0, $result->errorCode);
    }
}
