<?php

namespace ServiceCore\Notification\Tests\Transports\SmsTransport\Adapter;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Notification\Core\ServiceManager\SmsServiceManager;
use ServiceCore\Notification\Services\SmsService\Data\SmsSubAccount;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsTwilioAdapter;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Rest\Api\V2010\Account\MessageList;
use Twilio\Rest\Client as RestClient;

class SmsTwilioAdapterTest extends Test
{
    public function testsSendSms(): void
    {
        $body           = 'fakebody';
        $to             = '+18443360611';
        $from           = '+18443360612';
        $id             = 'thisIsAnId';
        $expectedParams = [];

        $messageInstanceStub = $this->createStub(MessageInstance::class);
        $messageInstanceStub->method('__get')->willReturn($id);

        $phoneNumberUtilStub = $this->createStub(PhoneNumberUtil::class);
        $phoneNumberUtilStub->method('parse')
            ->willReturn(new PhoneNumber());
        $phoneNumberUtilStub->method('format')
            ->willReturnOnConsecutiveCalls($to, $from);

        $messageListStub = $this->createMock(MessageList::class);
        $messageListStub->expects($this->once())
            ->method('create')
            ->with(
                $to,
                [
                    'From' => $from,
                    'Body' => $body
                ]
            )
            ->willReturn($messageInstanceStub);

        $restClientStub           = $this->createStub(RestClient::class);
        $restClientStub->messages = $messageListStub;

        $smsServiceManagerStub = $this->createStub(SmsServiceManager::class);
        $smsServiceManagerStub->expects($this->once())
            ->method('build')
            ->with(RestClient::class, $expectedParams)
            ->willReturn($restClientStub);

        $smsTwilioAdapter = new SmsTwilioAdapter($smsServiceManagerStub, $phoneNumberUtilStub);

        $result = $smsTwilioAdapter->sendSms(
            $to,
            $from,
            $body
        );

        $this->assertEquals($id, $result->sid);
    }

    public function testsSendSmsWithCredentials(): void
    {
        $body             = 'fakebody';
        $to               = '+18443360611';
        $from             = '+18443360612';
        $id               = 'thisIsAnId';
        $accountId        = 'anAccountId';
        $accountAuthToken = 'anAuthToken';
        $expectedParams   =
            [
                'accountId'    => $accountId,
                'accountToken' => $accountAuthToken
            ];

        $messageInstanceStub = $this->createStub(MessageInstance::class);
        $messageInstanceStub->method('__get')->willReturn($id);

        $phoneNumberUtilStub = $this->createStub(PhoneNumberUtil::class);
        $phoneNumberUtilStub->method('parse')
            ->willReturn(new PhoneNumber());
        $phoneNumberUtilStub->method('format')
            ->willReturnOnConsecutiveCalls($to, $from);

        $messageListStub = $this->createMock(MessageList::class);
        $messageListStub->expects($this->once())
            ->method('create')
            ->with(
                $to,
                [
                    'From' => $from,
                    'Body' => $body
                ]
            )
            ->willReturn($messageInstanceStub);

        $restClientStub           = $this->createStub(RestClient::class);
        $restClientStub->messages = $messageListStub;

        $smsServiceManagerStub = $this->createStub(SmsServiceManager::class);
        $smsServiceManagerStub->expects($this->once())
            ->method('build')
            ->with(RestClient::class, $expectedParams)
            ->willReturn($restClientStub);

        $smsTwilioAdapter = new SmsTwilioAdapter($smsServiceManagerStub, $phoneNumberUtilStub);

        $result = $smsTwilioAdapter->sendSms(
            $to,
            $from,
            $body,
            new SmsSubAccount($accountId, $accountAuthToken)
        );

        $this->assertEquals($id, $result->sid);
    }

    public function testSendSmsWithMessageServiceSid(): void
    {
        $body           = 'fakebody';
        $to             = '+18443360611';
        $from           = 'MSAbc123YouAndMeSid';
        $id             = 'thisIsAnId';
        $expectedParams = [];

        $messageInstanceStub = $this->createStub(MessageInstance::class);
        $messageInstanceStub->method('__get')->willReturn($id);

        $phoneNumberUtilStub = $this->createStub(PhoneNumberUtil::class);
        $phoneNumberUtilStub->method('parse')
            ->willReturnOnConsecutiveCalls(
                new PhoneNumber(),
                $this->throwException(
                    new NumberParseException(NumberParseException::NOT_A_NUMBER, 'oops')
                )
            );
        $phoneNumberUtilStub->method('format')
            ->willReturn($to);

        $messageListStub = $this->createMock(MessageList::class);
        $messageListStub->expects($this->once())
            ->method('create')
            ->with(
                $to,
                [
                    'From' => $from,
                    'Body' => $body
                ]
            )
            ->willReturn($messageInstanceStub);

        $restClientStub           = $this->createStub(RestClient::class);
        $restClientStub->messages = $messageListStub;

        $smsServiceManagerStub = $this->createStub(SmsServiceManager::class);
        $smsServiceManagerStub->expects($this->once())
            ->method('build')
            ->with(RestClient::class, $expectedParams)
            ->willReturn($restClientStub);

        $smsTwilioAdapter = new SmsTwilioAdapter($smsServiceManagerStub, $phoneNumberUtilStub);

        $result = $smsTwilioAdapter->sendSms(
            $to,
            $from,
            $body
        );

        $this->assertEquals($id, $result->sid);
    }

    public function testsSendSmsHandlesTwilioException(): void
    {
        $body           = 'fakebody';
        $to             = '+18443360611';
        $from           = '+18443360612';
        $errorCode      = 12345;
        $errorMessage   = 'ohNoOurTableItsBroken';
        $expectedParams = [];

        $restExceptionStub = new RestException($errorMessage, $errorCode, 400);

        $phoneNumberUtilStub = $this->createStub(PhoneNumberUtil::class);
        $phoneNumberUtilStub->method('parse')
            ->willReturn(new PhoneNumber());
        $phoneNumberUtilStub->method('format')
            ->willReturnOnConsecutiveCalls($to, $from);

        $messageListStub = $this->createMock(MessageList::class);
        $messageListStub->expects($this->once())
            ->method('create')
            ->with(
                $to,
                [
                    'From' => $from,
                    'Body' => $body
                ]
            )
            ->willThrowException($restExceptionStub);

        $restClientStub           = $this->createStub(RestClient::class);
        $restClientStub->messages = $messageListStub;

        $smsServiceManagerStub = $this->createStub(SmsServiceManager::class);
        $smsServiceManagerStub->expects($this->once())
            ->method('build')
            ->with(RestClient::class, $expectedParams)
            ->willReturn($restClientStub);

        $smsTwilioAdapter = new SmsTwilioAdapter($smsServiceManagerStub, $phoneNumberUtilStub);

        $result = $smsTwilioAdapter->sendSms($to, $from, $body);

        $this->assertNull($result->sid);
        $this->assertEquals($errorMessage, $result->errorMessage);
        $this->assertEquals($errorCode, $result->errorCode);
    }
}
