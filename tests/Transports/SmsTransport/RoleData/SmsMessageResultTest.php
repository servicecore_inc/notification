<?php

namespace ServiceCore\Notification\Tests\Transports\SmsTransport\RoleData;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;

class SmsMessageResultTest extends Test
{
    public function testMagicGet(): void
    {
        $properties = [
            'test1' => 'larry',
            'test2' => 'Curly',
            'test3' => 'Moe'
        ];

        $smsMessageResult = new SmsMessageResult(
            1234,
            null,
            null,
            $properties
        );

        $this->assertEquals($properties['test1'], $smsMessageResult->test1);
        $this->assertEquals($properties['test2'], $smsMessageResult->test2);
        $this->assertEquals($properties['test3'], $smsMessageResult->test3);
    }
}
