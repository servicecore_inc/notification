<?php

namespace ServiceCore\Notification\Tests\Transports\Core\Context;

use Laminas\EventManager\EventManagerInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\Notification\Core\Data\Notification;
use ServiceCore\Notification\Transports\Core\Context\TransportNotificationContext;

class TransportNotificationContextTest extends TestCase
{
    public function testNotifyByTransportFlag(): void
    {
        $notification = $this->createStub(Notification::class);
        $eventManager = $this->createMock(EventManagerInterface::class);
        $context      = new TransportNotificationContext($notification, $eventManager);
        $flag         = 0b00;
        $scriptsPath  = '/foo';
        $layoutsPath  = '/bar';

        $eventManager->expects($this->once())
                     ->method('trigger')
                     ->with(
                         'notification.transport',
                         $context,
                         [
                             'notification'     => $notification,
                             'scriptsPath'      => $scriptsPath,
                             'layoutsPath'      => $layoutsPath,
                             'notificationType' => $flag
                         ]
                     );

        $context->notifyByTransportFlag($flag, $scriptsPath, $layoutsPath);
    }
}
