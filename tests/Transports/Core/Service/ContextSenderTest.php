<?php

namespace ServiceCore\Notification\Tests\Transports\Core\Service;

use Laminas\EventManager\EventManagerInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\Notification\Core\Data\Notification;
use ServiceCore\Notification\Transports\Core\Context\TransportNotificationContext;
use ServiceCore\Notification\Transports\Core\Service\ContextSender;

class ContextSenderTest extends TestCase
{
    public function testSendOneByTransportFlag(): void
    {
        $eventManager = $this->createMock(EventManagerInterface::class);
        $notification = $this->createStub(Notification::class);
        $scriptsPath  = '/foo';
        $layoutsPath  = '/bar';
        $flag         = 0b00;

        $transportNotification = new TransportNotificationContext($notification, $eventManager);

        $service = new ContextSender($eventManager, $scriptsPath, $layoutsPath);

        $eventManager->expects(self::once())
            ->method('trigger')
            ->with(
                'notification.transport',
                $transportNotification,
                [
                    'notification'     => $notification,
                    'scriptsPath'      => $scriptsPath,
                    'layoutsPath'      => $layoutsPath,
                    'notificationType' => $flag,
                ]
            );

        $service->sendOneByTransportFlag($notification, $flag);
    }
}
