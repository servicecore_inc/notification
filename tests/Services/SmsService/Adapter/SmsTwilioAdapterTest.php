<?php

namespace ServiceCore\Notification\Tests\Services\SmsService\Adapter;

use Laminas\ServiceManager\ServiceLocatorInterface;
use PHPUnit\Framework\TestCase as Test;
use RuntimeException;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsTwilioAdapter;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessIdentity;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessIndustry;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessRegistrationIdentity;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessType;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioCompanyType;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioEventSinkType;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioJobLevel;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioRegion;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioUseCase;
use Twilio\Rest\Api;
use Twilio\Rest\Api\V2010;
use Twilio\Rest\Api\V2010\Account\AddressInstance;
use Twilio\Rest\Api\V2010\Account\AddressList;
use Twilio\Rest\Api\V2010\Account\AvailablePhoneNumberCountry\LocalInstance;
use Twilio\Rest\Api\V2010\Account\AvailablePhoneNumberCountry\LocalList;
use Twilio\Rest\Api\V2010\Account\AvailablePhoneNumberCountryContext;
use Twilio\Rest\Api\V2010\Account\IncomingPhoneNumberInstance;
use Twilio\Rest\Api\V2010\Account\IncomingPhoneNumberList;
use Twilio\Rest\Api\V2010\AccountContext;
use Twilio\Rest\Api\V2010\AccountInstance;
use Twilio\Rest\Api\V2010\AccountList;
use Twilio\Rest\Client as RestClient;
use Twilio\Rest\Events;
use Twilio\Rest\Events\V1 as EventsV1;
use Twilio\Rest\Events\V1\SinkInstance;
use Twilio\Rest\Events\V1\SinkList;
use Twilio\Rest\Events\V1\SubscriptionInstance;
use Twilio\Rest\Events\V1\SubscriptionList;
use Twilio\Rest\Messaging;
use Twilio\Rest\Messaging\V1 as MessagingV1;
use Twilio\Rest\Messaging\V1\BrandRegistrationList;
use Twilio\Rest\Messaging\V1\Service\PhoneNumberInstance;
use Twilio\Rest\Messaging\V1\Service\PhoneNumberList;
use Twilio\Rest\Messaging\V1\Service\UsAppToPersonInstance;
use Twilio\Rest\Messaging\V1\Service\UsAppToPersonList;
use Twilio\Rest\Messaging\V1\ServiceContext;
use Twilio\Rest\Messaging\V1\ServiceInstance;
use Twilio\Rest\Messaging\V1\ServiceList;
use Twilio\Rest\Studio;
use Twilio\Rest\Studio\V2;
use Twilio\Rest\Studio\V2\FlowInstance;
use Twilio\Rest\Studio\V2\FlowList;
use Twilio\Rest\Trusthub;
use Twilio\Rest\Trusthub\V1;
use Twilio\Rest\Trusthub\V1\CustomerProfiles\CustomerProfilesEntityAssignmentsList;
use Twilio\Rest\Trusthub\V1\CustomerProfiles\CustomerProfilesEvaluationsInstance;
use Twilio\Rest\Trusthub\V1\CustomerProfiles\CustomerProfilesEvaluationsList;
use Twilio\Rest\Trusthub\V1\CustomerProfilesContext;
use Twilio\Rest\Trusthub\V1\CustomerProfilesInstance;
use Twilio\Rest\Trusthub\V1\CustomerProfilesList;
use Twilio\Rest\Trusthub\V1\EndUserInstance;
use Twilio\Rest\Trusthub\V1\EndUserList;
use Twilio\Rest\Trusthub\V1\SupportingDocumentInstance;
use Twilio\Rest\Trusthub\V1\SupportingDocumentList;
use Twilio\Rest\Trusthub\V1\TrustProducts\TrustProductsEntityAssignmentsList;
use Twilio\Rest\Trusthub\V1\TrustProducts\TrustProductsEvaluationsInstance;
use Twilio\Rest\Trusthub\V1\TrustProducts\TrustProductsEvaluationsList;
use Twilio\Rest\Trusthub\V1\TrustProductsContext;
use Twilio\Rest\Trusthub\V1\TrustProductsInstance;
use Twilio\Rest\Trusthub\V1\TrustProductsList;

/**
 * @group sms
 */
class SmsTwilioAdapterTest extends Test
{
    public function testRegisterBrand(): void
    {
        $properties = [
            'companyName'                    => "Testy McTesterson's Testers",
            'companyAddress'                 => '405 Urban St',
            'companySecondaryAddress'        => 'suit 200',
            'companyCity'                    => 'Denver',
            'companyState'                   => 'CO',
            'companyZipCode'                 => '80830',
            'companyCountry'                 => 'US',
            'companyEmail'                   => 'tester@McTest.com',
            'companyType'                    => SmsTwilioCompanyType::PRIVATE,
            'socialMediaTag'                 => '@testy',
            'profileStatusCallbackUrl'       => 'https://callback.tester.com',
            'website'                        => 'https://tester.com',
            'regions'                        => SmsTwilioRegion::USA_AND_CANADA,
            'businessType'                   => SmsTwilioBusinessType::LIMITED_LIABILITY_CORPORATION,
            'businessIdentity'               => SmsTwilioBusinessIdentity::DIRECT_CUSTOMER,
            'businessIndustry'               => SmsTwilioBusinessIndustry::PROFESSIONAL_SERVICES,
            'businessRegistrationIdentifier' => SmsTwilioBusinessRegistrationIdentity::EIN,
            'businessRegistrationNumber'     => 'register1234asNumber',
            'ownerName'                      => 'Julio Verne',
            'ownerFirstName'                 => 'Julio',
            'ownerLastName'                  => 'Verne',
            'ownerPhone'                     => '+16267873612',
            'ownerEmail'                     => 'juliovernem@example.com',
            'ownerPosition'                  => SmsTwilioJobLevel::VP,
            'webhookUrl'                     => 'https://callback.tester.com',
            'skipAutomaticSecVet'            => true,
            'isMock'                         => true,
        ];

        $mockConfig = [
            'twilio' => [
                'primaryBundleSid' => 'primaryBundleSid'
            ]
        ];

        $accountInstanceStub            = $this->createStub(AccountInstance::class);
        $accountInstanceStub->sid       = 'subAccountId';
        $accountInstanceStub->authToken = 'subAccountAuthToken';

        $addressInstanceStub      = $this->createStub(AddressInstance::class);
        $addressInstanceStub->sid = 'addressInstanceStub';

        $supportingDocumentInstanceStub      = $this->createStub(SupportingDocumentInstance::class);
        $supportingDocumentInstanceStub->sid = 'addressSupportinDocumentInstanceStubSid';

        $customerProfilesInstanceStub      = $this->createStub(CustomerProfilesInstance::class);
        $customerProfilesInstanceStub->sid = 'customerProfileInstanceSid';

        $a2pTrustProductsInstanceStub      = $this->createStub(TrustProductsInstance::class);
        $a2pTrustProductsInstanceStub->sid = 'a2pTrustProductInstanceSid';

        $customerEvaluationInstanceMock         = $this->createMock(
            CustomerProfilesEvaluationsInstance::class
        );
        $customerEvaluationInstanceMock->status = SmsTwilioAdapter::EVALUATION_COMPLIANT;

        $trustProductsEvaluationInstanceMock         = $this->createMock(
            TrustProductsEvaluationsInstance::class
        );
        $trustProductsEvaluationInstanceMock->status = SmsTwilioAdapter::EVALUATION_COMPLIANT;

        $customerBusinessInfoStub      = $this->createStub(EndUserInstance::class);
        $customerBusinessInfoStub->sid = 'customerBusinessInfo';

        $authorizedRepresentativeStub      = $this->createStub(EndUserInstance::class);
        $authorizedRepresentativeStub->sid = 'authorizedRepresentative';

        $usA2PMessagingProfileStub      = $this->createStub(EndUserInstance::class);
        $usA2PMessagingProfileStub->sid = 'usA2PMessagingProfileSid';

        $customerProfilesContextMock                                    = $this->createMock(
            CustomerProfilesContext::class
        );
        $customerProfilesContextMock->customerProfilesEntityAssignments = $this->createMock(
            CustomerProfilesEntityAssignmentsList::class
        );
        $customerProfilesContextMock->customerProfilesEntityAssignments
            ->expects($this::exactly(4))
            ->method('create')
            ->with(
                $this->callback(static function (string $id) use (
                    $mockConfig,
                    $customerBusinessInfoStub,
                    $authorizedRepresentativeStub,
                    $supportingDocumentInstanceStub
                ) {
                    switch ($id) {
                        case $customerBusinessInfoStub->sid:
                        case $authorizedRepresentativeStub->sid:
                        case $supportingDocumentInstanceStub->sid:
                        case $mockConfig['twilio']['primaryBundleSid']:
                            return true;
                        default:
                            return false;
                    }
                })
            );

        $customerProfilesContextMock->customerProfilesEvaluations = $this->createMock(
            CustomerProfilesEvaluationsList::class
        );
        $customerProfilesContextMock->customerProfilesEvaluations
            ->expects($this::once())
            ->method('create')
            ->with(SmsTwilioAdapter::SECONDARY_CUSTOMER_PROFILE_GLOBAL_POLICY_SID)
            ->willReturn($customerEvaluationInstanceMock);

        $customerProfilesContextMock->expects($this::once())->method('update')->with([
            'status' => 'pending-review'
        ])->willReturn($customerProfilesInstanceStub);

        $trustProductsContextMock = $this->createMock(TrustProductsContext::class);

        $trustProductsContextMock->expects(self::once())->method('update')->with([
            'status' => 'pending-review'
        ])->willReturn($a2pTrustProductsInstanceStub);

        $trustProductsContextMock->trustProductsEntityAssignments
            = $this->createMock(TrustProductsEntityAssignmentsList::class);
        $trustProductsContextMock->trustProductsEntityAssignments
            ->expects($this::exactly(2))
            ->method('create')
            ->with(
                $this->callback(static function (string $id) use (
                    $usA2PMessagingProfileStub,
                    $customerProfilesInstanceStub
                ) {
                    switch ($id) {
                        case $usA2PMessagingProfileStub->sid:
                        case $customerProfilesInstanceStub->sid:
                            return true;
                        default:
                            return false;
                    }
                })
            );

        $trustProductsContextMock->trustProductsEvaluations =
            $this->createMock(TrustProductsEvaluationsList::class);

        $trustProductsContextMock->trustProductsEvaluations
            ->expects(self::once())
            ->method('create')
            ->with(SmsTwilioAdapter::A2P_MESSAGING_TRUST_HUB_POLICY_SID)
            ->willReturn($trustProductsEvaluationInstanceMock);

        $restClient                       = $this->createStub(RestClient::class);
        $restClient->api                  = $this->createStub(Api::class);
        $restClient->api->v2010           = $this->createStub(V2010::class);
        $restClient->api->v2010->accounts = $this->createMock(AccountList::class);
        $restClient->api->v2010->accounts
            ->expects($this::once())
            ->method('create')
            ->with(['friendlyName' => $properties['companyName']])
            ->willReturn($accountInstanceStub);

        $subClient               = $this->createStub(RestClient::class);
        $subClient->trusthub     = $this->createStub(Trusthub::class);
        $subClient->trusthub->v1 = $this->createMock(v1::class);

        $subClient->trusthub->v1->customerProfiles = $this->createMock(CustomerProfilesList::class);
        $subClient->trusthub->v1->customerProfiles
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['companyName'],
                $properties['companyEmail'],
                SmsTwilioAdapter::SECONDARY_CUSTOMER_PROFILE_GLOBAL_POLICY_SID,
                [
                    'statusCallback' => $properties['profileStatusCallbackUrl']
                ]
            )
            ->willReturn($customerProfilesInstanceStub);

        $trustHubV1CallMatcher = $this->exactly(2);
        $subClient->trusthub->v1
            ->expects($trustHubV1CallMatcher)
            ->method('__call')
            ->willReturnCallback(
                function (string $contextMethodName) use (
                    $trustHubV1CallMatcher,
                    $customerProfilesContextMock,
                    $trustProductsContextMock
                ) {
                    switch ($trustHubV1CallMatcher->getInvocationCount()) {
                        case 1:
                            $this->assertEquals('customerProfiles', $contextMethodName);

                            return $customerProfilesContextMock;
                        case 2:
                            $this->assertEquals('trustProducts', $contextMethodName);

                            return $trustProductsContextMock;
                    }
                }
            );

        $endUsersMatcher                   = $this->exactly(3);
        $subClient->trusthub->v1->endUsers = $this->createMock(EndUserList::class);
        $subClient->trusthub->v1->endUsers
            ->expects($endUsersMatcher)
            ->method('create')
            ->willReturnCallback(
                function (
                    string $friendlyName,
                    string $type,
                    array $options
                ) use (
                    $endUsersMatcher,
                    $properties,
                    $customerBusinessInfoStub,
                    $authorizedRepresentativeStub,
                    $usA2PMessagingProfileStub
                ) {
                    switch ($endUsersMatcher->getInvocationCount()) {
                        case 1:
                            $this->assertEquals($properties['companyName'], $friendlyName);
                            $this->assertEquals(
                                SmsTwilioAdapter::END_USER_CUSTOMER_PROFILE_BUSINESS_INFO_TYPE,
                                $type
                            );
                            $this->assertEquals(
                                [
                                    'attributes' => [
                                        'business_name'                    => $properties['companyName'],
                                        'social_media_profile_urls'        => $properties['socialMediaTag'],
                                        'website_url'                      => $properties['website'],
                                        'business_regions_of_operation'    => $properties['regions'],
                                        'business_type'                    => $properties['businessType'],
                                        'business_registration_identifier' =>
                                            $properties['businessRegistrationIdentifier'],
                                        'business_identity'                => $properties['businessIdentity'],
                                        'business_industry'                => $properties['businessIndustry'],
                                        'business_registration_number'     => $properties['businessRegistrationNumber']
                                    ],
                                ],
                                $options
                            );

                            return $customerBusinessInfoStub;
                        case 2:
                            $this->assertEquals($properties['ownerName'], $friendlyName);
                            $this->assertEquals(SmsTwilioAdapter::AUTHORIZED_REPRESENTATIVE_TYPE, $type);
                            $this->assertEquals(
                                [
                                    'attributes' => [
                                        'first_name'     => $properties['ownerFirstName'],
                                        'last_name'      => $properties['ownerLastName'],
                                        'phone_number'   => $properties['ownerPhone'],
                                        'email'          => $properties['ownerEmail'],
                                        'job_position'   => $properties['ownerPosition'],
                                        'business_title' => $properties['ownerPosition'],
                                    ]
                                ],
                                $options
                            );

                            return $authorizedRepresentativeStub;
                        case 3:
                            $this->assertEquals($properties['companyName'], $friendlyName);
                            $this->assertEquals(SmsTwilioAdapter::END_USER_US_A2P_MESSAGING_PROFILE, $type);
                            $this->assertEquals(
                                [
                                    'attributes' => [
                                        'company_type' => 'private'
                                    ]
                                ],
                                $options
                            );

                            return $usA2PMessagingProfileStub;
                    }

                    throw new RuntimeException('Could not map call count');
                }
            );

        $subClient->addresses = $this->createMock(AddressList::class);
        $subClient->addresses
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['companyName'],
                $properties['companyAddress'],
                $properties['companyCity'],
                $properties['companyState'],
                $properties['companyZipCode'],
                $properties['companyCountry'],
                [
                    'friendlyName'    => $properties['companyName'],
                    'streetSecondary' => $properties['companySecondaryAddress']
                ],
            )
            ->willReturn($addressInstanceStub);

        $subClient->trusthub->v1->supportingDocuments = $this->createMock(SupportingDocumentList::class);
        $subClient->trusthub->v1->supportingDocuments
            ->expects($this->once())
            ->method('create')
            ->with(
                $properties['companyName'],
                SmsTwilioAdapter::ADDRESS_DOCUMENTS_TYPE,
                [
                    'attributes' => [
                        'address_sids' => $addressInstanceStub->sid
                    ]
                ]
            )->willReturn($supportingDocumentInstanceStub);

        $containerMatcher = $this->exactly(2);
        $container        = $this->createMock(ServiceLocatorInterface::class);

        $container->expects($this::once())
                  ->method('get')
                  ->with('config')
                  ->willReturn($mockConfig);

        $buildCallback = function (
            $name,
            $options
        ) use (
            $containerMatcher,
            $restClient,
            $subClient,
            $accountInstanceStub
        ) {
            switch ($containerMatcher->getInvocationCount()) {
                case 1:
                    $this->assertEquals(RestClient::class, $name);
                    $this->assertNull($options);

                    return $restClient;
                case 2:
                    $this->assertEquals(RestClient::class, $name);
                    $this->assertEquals(
                        [
                            'accountId'    => $accountInstanceStub->sid,
                            'accountToken' => $accountInstanceStub->authToken
                        ],
                        $options
                    );

                    return $subClient;
            }

            throw new RuntimeException('Could not map call count');
        };

        $container->expects($containerMatcher)
                  ->method('build')
                  ->willReturnCallback($buildCallback);

        $trustProductsInstanceStub      = $this->createStub(TrustProductsInstance::class);
        $trustProductsInstanceStub->sid = 'trustProductsInstanceSid';

        $sinkInstanceStub      = $this->createStub(SinkInstance::class);
        $sinkInstanceStub->sid = 'sinkInstanceSid';

        $brandRegistrationInstanceStub             = $this->createStub(
            MessagingV1\BrandRegistrationInstance::class
        );
        $brandRegistrationInstanceStub->sid        = 'brandRegistrationInstanceSid';
        $brandRegistrationInstanceStub->accountSid = 'brandRegistrationInstanceAccountSid';

        $subscriptionInstanceStub = $this->createStub(SubscriptionInstance::class);

        $subClient->trusthub->v1->trustProducts = $this->createMock(TrustProductsList::class);
        $subClient->trusthub->v1->trustProducts
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['companyName'],
                $properties['companyEmail'],
                SmsTwilioAdapter::A2P_MESSAGING_TRUST_HUB_POLICY_SID
            )
            ->willReturn($trustProductsInstanceStub);

        $subClient->events     = $this->createStub(Events::class);
        $subClient->events->v1 = $this->createMock(EventsV1::class);

        $subClient->events->v1->sinks = $this->createMock(SinkList::class);
        $subClient->events->v1->sinks
            ->expects($this::once())
            ->method('create')
            ->with(
                SmsTwilioAdapter::REGISTRATION_SINK,
                [
                    'destination'  => $properties['webhookUrl'],
                    'method'       => 'POST',
                    'batch_events' => false
                ],
                'webhook'
            )
            ->willReturn($sinkInstanceStub);

        $subClient->events->v1->subscriptions = $this->createMock(SubscriptionList::class);
        $subClient->events->v1->subscriptions
            ->expects($this::once())
            ->method('create')
            ->with(
                SmsTwilioAdapter::REGISTRATION_SUBSCRIPTION,
                $sinkInstanceStub->sid,
                [
                    [
                        'type' => SmsTwilioEventSinkType::BRAND_REGISTRATION_FAILURE
                    ],
                    [
                        'type' => SmsTwilioEventSinkType::BRAND_REGISTRATION_REGISTERED,
                    ],
                    [
                        'type' => SmsTwilioEventSinkType::BRAND_REGISTRATION_UNVERIFIED,
                    ],
                    [
                        'type' => SmsTwilioEventSinkType::CAMPAIGN_REGISTRATION_APPROVED,
                    ],
                    [
                        'type' => SmsTwilioEventSinkType::CAMPAIGN_REGISTRATION_SUBMITTED,
                    ],
                    [
                        'type' => SmsTwilioEventSinkType::CAMPAIGN_REGISTRATION_FAILURE,
                    ]
                ]
            )
            ->willReturn($subscriptionInstanceStub);

        $subClient->messaging     = $this->createStub(Messaging::class);
        $subClient->messaging->v1 = $this->createStub(MessagingV1::class);

        $subClient->messaging->v1->brandRegistrations = $this->createMock(BrandRegistrationList::class);
        $subClient->messaging->v1->brandRegistrations
            ->expects($this::once())
            ->method('create')
            ->with($customerProfilesInstanceStub->sid, $a2pTrustProductsInstanceStub->sid)
            ->willReturn($brandRegistrationInstanceStub);

        $adapter = new SmsTwilioAdapter($container);

        $adapter->registerBrand($properties);
    }

    public function testCreateCampaign(): void
    {
        $properties = [
            'companyName'          => "Testy McTesterson's Testers",
            'accountSid'           => 'subAccountSid',
            'statusWebhookUrl'     => 'https://fakeWebhook.servicecore.invalid',
            'useCase'              => SmsTwilioUseCase::DELIVERY_NOTIFICATION_USE_CASE,
            'brandRegistrationSid' => 'BN09190293091028883990403830921092',
            'description'          => 'Send notification messages',
            'messageFlow'          => 'test message flow',
            'messageSamples'       => [
                'Driver is finishing a delivery and your site is the next in queue',
                'Driver is on the way to your site'
            ],
            'hasEmbeddedLinks'     => false,
            'hasEmbeddedPhone'     => false,
            'optInKeyWords'        => ['START'],
            'optInMessage'         => 'Now you are subscribed to our notifications',
            'optOutKeyWords'       => ['STOP', 'STOPALL'],
            'optOutMessage'        => "You are now unsubscribed and we won't send you notifications",
            'flowName'             => 'testFlow',
            'flowDefinition'       => ['name' => 'testFlowDef'],
        ];

        $accountInstanceStub            = $this->createStub(AccountInstance::class);
        $accountInstanceStub->sid       = 'accountInstanceStubSid';
        $accountInstanceStub->authToken = 'accountInstanceStubToken';

        $incomingPhoneNumberInstanceStub      = $this->createStub(IncomingPhoneNumberInstance::class);
        $incomingPhoneNumberInstanceStub->sid = 'incomingPhoneNumberInstanceStubSid';

        $serviceInstanceStub      = $this->createStub(ServiceInstance::class);
        $serviceInstanceStub->sid = 'serviceInstanceStubSid';

        $phoneNumberInstanceStub = $this->createStub(PhoneNumberInstance::class);

        $localPhoneInstanceStub              = $this->createStub(LocalInstance::class);
        $localPhoneInstanceStub->phoneNumber = '+12909309293';

        $usAppToPersonInstanceStub      = $this->createStub(UsAppToPersonInstance::class);
        $usAppToPersonInstanceStub->sid = 'fooBar';

        $flowInstanceStub             = $this->createStub(FlowInstance::class);
        $flowInstanceStub->webhookUrl = 'flowInstanceUrl';

        $accountContextMock = $this->createMock(AccountContext::class);

        $accountContextMock->expects($this::once())
                           ->method('fetch')
                           ->willReturn($accountInstanceStub);

        $availablePhoneNumberCountryContext = $this->createStub(
            AvailablePhoneNumberCountryContext::class
        );

        $availablePhoneNumberCountryContext->local = $this->createStub(LocalList::class);
        $availablePhoneNumberCountryContext->local
            ->expects($this::once())
            ->method('read')
            ->willReturn([$localPhoneInstanceStub]);

        $restClient             = $this->createStub(RestClient::class);
        $restClient->api        = $this->createStub(Api::class);
        $restClient->api->v2010 = $this->createMock(V2010::class);

        $restClient->api->v2010
            ->expects($this::once())
            ->method('__call')
            ->with('accounts')
            ->willReturn($accountContextMock);

        $subClient = $this->createMock(RestClient::class);
        $subClient
            ->expects($this->once())
            ->method('__call')
            ->willReturn($availablePhoneNumberCountryContext);

        $subClient->incomingPhoneNumbers = $this->createMock(IncomingPhoneNumberList::class);
        $subClient->incomingPhoneNumbers
            ->expects($this::once())
            ->method('create')
            ->with([
                'phoneNumber' => $localPhoneInstanceStub->phoneNumber,
                'smsUrl'      => $flowInstanceStub->webhookUrl
            ])
            ->willReturn(
                $incomingPhoneNumberInstanceStub
            );


        $subClient->messaging               = $this->createStub(Messaging::class);
        $subClient->messaging->v1           = $this->createMock(MessagingV1::class);
        $subClient->messaging->v1->services = $this->createMock(ServiceList::class);
        $subClient->messaging->v1->services
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['companyName'],
                [
                    'statusCallback'            => $properties['statusWebhookUrl'],
                    'stickySender'              => true,
                    'UseInboundWebhookOnNumber' => true
                ]
            )->willReturn($serviceInstanceStub);


        $messagingServiceContextStub = $this->createStub(ServiceContext::class);

        $messagingServiceContextStub->phoneNumbers = $this->createMock(PhoneNumberList::class);
        $messagingServiceContextStub->phoneNumbers
            ->expects($this::once())
            ->method('create')
            ->with($incomingPhoneNumberInstanceStub->sid)
            ->willReturn($phoneNumberInstanceStub);

        $messagingServiceContextStub->usAppToPerson = $this->createMock(UsAppToPersonList::class);
        $messagingServiceContextStub->usAppToPerson
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['brandRegistrationSid'],
                $properties['description'],
                $properties['messageFlow'],
                $properties['messageSamples'],
                $properties['useCase'],
                $properties['hasEmbeddedLinks'],
                $properties['hasEmbeddedPhone'],
                [
                    'optInKeywords'  => $properties['optInKeyWords'],
                    'optInMessage'   => $properties['optInMessage'],
                    'optOutKeywords' => $properties['optOutKeyWords'],
                    'optOutMessage'  => $properties['optOutMessage']
                ],
            )
            ->willReturn($usAppToPersonInstanceStub);

        $subClient->messaging->v1
            ->expects($this::exactly(2))
            ->method('__call')
            ->with('services')
            ->willReturn($messagingServiceContextStub);

        $containerMatcher = $this->exactly(2);
        $buildCallback    = function (
            $className,
            $options
        ) use (
            $restClient,
            $subClient,
            $containerMatcher,
            $accountInstanceStub
        ) {
            switch ($containerMatcher->getInvocationCount()) {
                case 1:
                    $this->assertEquals(RestClient::class, $className);
                    $this->assertNull($options);

                    return $restClient;
                case 2:
                    $this->assertEquals(RestClient::class, $className);
                    $this->assertEquals(
                        [
                            'accountId'    => $accountInstanceStub->sid,
                            'accountToken' => $accountInstanceStub->authToken
                        ],
                        $options
                    );

                    return $subClient;
            }

            throw new RuntimeException('Could not map call count');
        };

        $container = $this->createMock(ServiceLocatorInterface::class);
        $container->expects($containerMatcher)
                  ->method('build')
                  ->willReturnCallback($buildCallback);

        $subClient->studio            = $this->createStub(Studio::class);
        $subClient->studio->v2        = $this->createStub(V2::class);
        $subClient->studio->v2->flows = $this->createMock(FlowList::class);
        $subClient->studio->v2->flows->expects($this->once())
        ->method('create')
        ->with(
            $properties['flowName'],
            'published',
            $properties['flowDefinition']
        )
        ->willReturn($flowInstanceStub);

        $adapter = new SmsTwilioAdapter($container);

        $adapter->createCampaign($properties);
    }

    public function testCreateCampaignWithoutFlow(): void
    {
        $properties = [
            'companyName'          => "Testy McTesterson's Testers",
            'accountSid'           => 'subAccountSid',
            'statusWebhookUrl'     => 'https://fakeWebhook.servicecore.invalid',
            'useCase'              => SmsTwilioUseCase::DELIVERY_NOTIFICATION_USE_CASE,
            'brandRegistrationSid' => 'BN09190293091028883990403830921092',
            'description'          => 'Send notification messages',
            'messageFlow'          => 'test message flow',
            'messageSamples'       => [
                'Driver is finishing a delivery and your site is the next in queue',
                'Driver is on the way to your site'
            ],
            'hasEmbeddedLinks'     => false,
            'hasEmbeddedPhone'     => false,
            'optInKeyWords'        => ['START'],
            'optInMessage'         => 'Now you are subscribed to our notifications',
            'optOutKeyWords'       => ['STOP', 'STOPALL'],
            'optOutMessage'        => "You are now unsubscribed and we won't send you notifications",
        ];

        $accountInstanceStub            = $this->createStub(AccountInstance::class);
        $accountInstanceStub->sid       = 'accountInstanceStubSid';
        $accountInstanceStub->authToken = 'accountInstanceStubToken';

        $incomingPhoneNumberInstanceStub      = $this->createStub(IncomingPhoneNumberInstance::class);
        $incomingPhoneNumberInstanceStub->sid = 'incomingPhoneNumberInstanceStubSid';

        $serviceInstanceStub      = $this->createStub(ServiceInstance::class);
        $serviceInstanceStub->sid = 'serviceInstanceStubSid';

        $phoneNumberInstanceStub = $this->createStub(PhoneNumberInstance::class);

        $localPhoneInstanceStub              = $this->createStub(LocalInstance::class);
        $localPhoneInstanceStub->phoneNumber = '+12909309293';

        $usAppToPersonInstanceStub      = $this->createStub(UsAppToPersonInstance::class);
        $usAppToPersonInstanceStub->sid = 'fooBar';

        $flowInstanceStub      = $this->createStub(FlowInstance::class);
        $flowInstanceStub->url = 'flowInstanceUrl';

        $accountContextMock = $this->createMock(AccountContext::class);

        $accountContextMock->expects($this::once())
            ->method('fetch')
            ->willReturn($accountInstanceStub);

        $availablePhoneNumberCountryContext = $this->createStub(
            AvailablePhoneNumberCountryContext::class
        );

        $availablePhoneNumberCountryContext->local = $this->createStub(LocalList::class);
        $availablePhoneNumberCountryContext->local
            ->expects($this::once())
            ->method('read')
            ->willReturn([$localPhoneInstanceStub]);

        $restClient             = $this->createStub(RestClient::class);
        $restClient->api        = $this->createStub(Api::class);
        $restClient->api->v2010 = $this->createMock(V2010::class);

        $restClient->api->v2010
            ->expects($this::once())
            ->method('__call')
            ->with('accounts')
            ->willReturn($accountContextMock);

        $subClient = $this->createMock(RestClient::class);
        $subClient
            ->expects($this->once())
            ->method('__call')
            ->willReturn($availablePhoneNumberCountryContext);

        $subClient->incomingPhoneNumbers = $this->createMock(IncomingPhoneNumberList::class);
        $subClient->incomingPhoneNumbers
            ->expects($this::once())
            ->method('create')
            ->with([
                'phoneNumber' => $localPhoneInstanceStub->phoneNumber
            ])
            ->willReturn(
                $incomingPhoneNumberInstanceStub
            );

        $subClient->messaging               = $this->createStub(Messaging::class);
        $subClient->messaging->v1           = $this->createMock(MessagingV1::class);
        $subClient->messaging->v1->services = $this->createMock(ServiceList::class);
        $subClient->messaging->v1->services
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['companyName'],
                [
                    'statusCallback'            => $properties['statusWebhookUrl'],
                    'stickySender'              => true,
                    'UseInboundWebhookOnNumber' => true,
                ]
            )->willReturn($serviceInstanceStub);

        $messagingServiceContextStub = $this->createStub(ServiceContext::class);

        $messagingServiceContextStub->phoneNumbers = $this->createMock(PhoneNumberList::class);
        $messagingServiceContextStub->phoneNumbers
            ->expects($this::once())
            ->method('create')
            ->with($incomingPhoneNumberInstanceStub->sid)
            ->willReturn($phoneNumberInstanceStub);

        $messagingServiceContextStub->usAppToPerson = $this->createMock(UsAppToPersonList::class);
        $messagingServiceContextStub->usAppToPerson
            ->expects($this::once())
            ->method('create')
            ->with(
                $properties['brandRegistrationSid'],
                $properties['description'],
                $properties['messageFlow'],
                $properties['messageSamples'],
                $properties['useCase'],
                $properties['hasEmbeddedLinks'],
                $properties['hasEmbeddedPhone'],
                [
                    'optInKeywords'  => $properties['optInKeyWords'],
                    'optInMessage'   => $properties['optInMessage'],
                    'optOutKeywords' => $properties['optOutKeyWords'],
                    'optOutMessage'  => $properties['optOutMessage']
                ],
            )
            ->willReturn($usAppToPersonInstanceStub);

        $subClient->messaging->v1
            ->expects($this::exactly(2))
            ->method('__call')
            ->with('services')
            ->willReturn($messagingServiceContextStub);

        $containerMatcher = $this->exactly(2);
        $buildCallback    = function (
            $className,
            $options
        ) use (
            $restClient,
            $subClient,
            $containerMatcher,
            $accountInstanceStub
        ) {
            switch ($containerMatcher->getInvocationCount()) {
                case 1:
                    $this->assertEquals(RestClient::class, $className);
                    $this->assertNull($options);

                    return $restClient;
                case 2:
                    $this->assertEquals(RestClient::class, $className);
                    $this->assertEquals(
                        [
                            'accountId'    => $accountInstanceStub->sid,
                            'accountToken' => $accountInstanceStub->authToken
                        ],
                        $options
                    );

                    return $subClient;
            }

            throw new RuntimeException('Could not map call count');
        };

        $container = $this->createMock(ServiceLocatorInterface::class);
        $container->expects($containerMatcher)
            ->method('build')
            ->willReturnCallback($buildCallback);

        $adapter = new SmsTwilioAdapter($container);

        $adapter->createCampaign($properties);
    }

    public function testGetAdapterName(): void
    {
        $restClient = $this->createStub(RestClient::class);
        $container  = $this->createMock(ServiceLocatorInterface::class);

        $container->expects($this::once())
            ->method('build')
            ->willReturn($restClient);

        $adapter = new SmsTwilioAdapter($container);

        $this->assertIsString(
            $adapter->getAdapterName()
        );
    }

    public function testFetchSubAccount(): void
    {
        $accountInstanceStub            = $this->createStub(AccountInstance::class);
        $accountInstanceStub->sid       = 'subAccountId';
        $accountInstanceStub->authToken = 'subAccountAuthToken';

        $accountContextMock = $this->createMock(AccountContext::class);

        $accountContextMock->expects($this::once())
            ->method('fetch')
            ->willReturn($accountInstanceStub);

        $restClient             = $this->createStub(RestClient::class);
        $restClient->api        = $this->createStub(Api::class);
        $restClient->api->v2010 = $this->createMock(V2010::class);

        $restClient->api->v2010
            ->expects($this::once())
            ->method('__call')
            ->with('accounts')
            ->willReturn($accountContextMock);


        $container = $this->createMock(ServiceLocatorInterface::class);
        $container->expects($this::once())
            ->method('build')
            ->willReturn($restClient);

        $adapter = new SmsTwilioAdapter($container);
        $adapter->fetchSubAccount($accountInstanceStub->sid);
    }
}
