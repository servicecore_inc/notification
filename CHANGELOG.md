# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 4.1.7
### Added
- SmsLogParams - added sender id for better logging

## 4.1.6
### Added
- Ability to set timeout for Twilio API requests

## 4.1.5
### Added
- SmsAccountInterface - Used to store related account identifier
- SmsAccountAwareInterface - Used to retrieve account identifier

### Changed
- SmsNotification - Can now retrieve account identifier

## 4.1.4
### Changed
- Service SmsTwilioAdapter - Can now create studio flows via array definition  

## 4.1.3
### Changed
- SmsTwilioAdapter - Can now pass account credentials to send messages via sub-accounts

## 4.1.2
### Changed
- SmsTwilioAdapter - Can now send sms with a message service sid

## 4.1.1
### Added
- SmsBrand - Added accountSid property

### Changed
- Service Sms Adapters - Inject Account Sid

## 4.1.0
### Added
- SMS brand/campaign registration
- Twilio adapter support
- Override adapter support
- SNS adapter stub

## 4.0.1
### Added
- SmsLogParams - Used to store parameters for a sms notification
- SmsNotification - Class to be extended by concrete SMS notifications
- SmsContactInterface - Used to provide getPhone() and hasSmsEnabled()

## 4.0.0
### Changed
- TransportSmsContext now returns an instance of SmsMessageResult
- Implemented SmsOverrideAdapter properly
