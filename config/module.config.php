<?php

use ServiceCore\Notification\Core\Factory\TwilioRestClientFactory;
use ServiceCore\Notification\Core\ServiceManager\Factory\SmsServiceManagerFactory;
use ServiceCore\Notification\Core\ServiceManager\SmsServiceManager;
use ServiceCore\Notification\Services\SmsService\Adapter\Factory\SmsOverrideAdapterFactory
    as SmsServiceOverrideAdapterFactory;
use ServiceCore\Notification\Services\SmsService\Adapter\Factory\SmsSnsAdapterFactory as SmsServiceSnsAdapterFactory;
use ServiceCore\Notification\Services\SmsService\Adapter\Factory\SmsTwilioAdapterFactory
    as SmsServiceTwilioAdapterFactory;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsOverrideAdapter as SmsServiceOverrideAdapter;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsSnsAdapter as SmsServiceSnsAdapter;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsTwilioAdapter as SmsServiceTwilioAdapter;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\Factory\SmsOverrideAdapterFactory as
    SmsTransportOverrideAdapterFactory;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\Factory\SmsSnsAdapterFactory as
    SmsTransportSnsAdapterFactory;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\Factory\SmsTwilioAdapterFactory as
    SmsTransportTwilioAdapterFactory;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsOverrideAdapter as SmsTransportOverrideAdapter;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsSnsAdapter as SmsTransportSnsAdapter;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsTwilioAdapter as SmsTransportTwilioAdapter;
use Twilio\Rest\Client;

return [
    'service_manager'     => [
        'factories' => [
            SmsServiceTwilioAdapter::class     => SmsServiceTwilioAdapterFactory::class,
            SmsTransportTwilioAdapter::class   => SmsTransportTwilioAdapterFactory::class,
            SmsServiceOverrideAdapter::class   => SmsServiceOverrideAdapterFactory::class,
            SmsTransportOverrideAdapter::class => SmsTransportOverrideAdapterFactory::class,
            SmsServiceSnsAdapter::class        => SmsServiceSnsAdapterFactory::class,
            SmsTransportSnsAdapter::class      => SmsTransportSnsAdapterFactory::class,
            SmsServiceManager::class           => SmsServiceManagerFactory::class
        ]
    ],
    'sms_service_manager' => [
        'factories' => [
            Client::class => TwilioRestClientFactory::class,
        ]
    ],
    'notification'        => [
        'sms'      => [
            'adapter' => 'override'
        ],
        'twilio'   => [
            'sid'              => null,
            'token'            => null,
            'service'          => SmsServiceTwilioAdapter::class,
            'transport'        => SmsTransportTwilioAdapter::class,
            'primaryBundleSid' => null,
            'timeout'          => 10
        ],
        'sns'      => [
            'service'   => SmsServiceSnsAdapter::class,
            'transport' => SmsTransportSnsAdapter::class,
        ],
        'override' => [
            'service'   => SmsServiceOverrideAdapter::class,
            'transport' => SmsTransportOverrideAdapter::class,
        ]
    ]
];
