<?php

namespace ServiceCore\Notification\Core\RoleData;

interface SmsAccountAwareInterface
{
    public function getNotificationSmsAccount(): ?SmsAccountInterface;
}
