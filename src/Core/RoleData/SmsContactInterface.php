<?php

namespace ServiceCore\Notification\Core\RoleData;

interface SmsContactInterface
{
    public function getPhone(): ?string;

    public function hasSmsEnabled(): bool;
}
