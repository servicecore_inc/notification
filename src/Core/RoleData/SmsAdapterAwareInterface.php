<?php

namespace ServiceCore\Notification\Core\RoleData;

use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface as SmsServiceAdapterInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsAdapterInterface as SmsTransportInterface;

interface SmsAdapterAwareInterface
{
    public function getSmsServiceAdapter(): SmsServiceAdapterInterface;

    public function setSmsServiceAdapter(SmsServiceAdapterInterface $smsServiceAdapter);

    public function getSmsTransportAdapter(): SmsTransportInterface;

    public function setSmsTransportAdapter(SmsTransportInterface $smsTransportAdapter);
}
