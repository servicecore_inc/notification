<?php

namespace ServiceCore\Notification\Core\RoleData;

/**
 * @author  Jason Campbell <jason@servicecore.com>
 */
interface SubscriptionProvider
{
    /**
     * @return int - expects a type of transport flag, such as:
     * @see TransportEmailContext::FLAG
     */
    public function getTransport(): int;
}
