<?php

namespace ServiceCore\Notification\Core\RoleData;

interface SmsAccountInterface
{
    public function getId();
}
