<?php

namespace ServiceCore\Notification\Core\Helper;

use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface as SmsServiceAdapterInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsAdapterInterface as SmsTransportAdapterInterface;

trait SmsAdapterAwareTrait
{
    private ?SmsServiceAdapterInterface $smsServiceAdapter     = null;
    private ?SmsTransportAdapterInterface $smsTransportAdapter = null;

    public function getSmsServiceAdapter(): SmsServiceAdapterInterface
    {
        if ($this->smsServiceAdapter === null) {
            throw new \RuntimeException('SmsServiceAdapter not set. Have you setup a delegator factory?');
        }

        return $this->smsServiceAdapter;
    }

    public function setSmsServiceAdapter(SmsServiceAdapterInterface $smsServiceAdapter): self
    {
        $this->smsServiceAdapter = $smsServiceAdapter;

        return $this;
    }

    public function getSmsTransportAdapter(): SmsTransportAdapterInterface
    {
        if ($this->smsTransportAdapter === null) {
            throw new \RuntimeException('SmsTransportAdapter not set. Have you setup a delegator factory?');
        }

        return $this->smsTransportAdapter;
    }

    public function setSmsTransportAdapter(SmsTransportAdapterInterface $smsTransportAdapter): self
    {
        $this->smsTransportAdapter = $smsTransportAdapter;

        return $this;
    }
}
