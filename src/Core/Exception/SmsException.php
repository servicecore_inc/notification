<?php

namespace ServiceCore\Notification\Core\Exception;

use Exception;
use Throwable;

class SmsException extends Exception
{
    public const SMS_ERROR = 'An error occurred while preforming an Sms operation: ';

    public function __construct(string $message, $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(\sprintf('%s %s', self::SMS_ERROR, $message), $code, $previous);
    }
}
