<?php

namespace ServiceCore\Notification\Core\Data;

use ServiceCore\Notification\Core\RoleData\SmsAccountAwareInterface;
use ServiceCore\Notification\Core\RoleData\SmsContactInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsTransporterData;

abstract class SmsNotification extends Notification implements SmsTransporterData
{
    private SmsContactInterface $smsContact;
    private SmsLogParams $smsLogParams;

    abstract public function hasSmsNotificationEnabled(): bool;

    abstract public function getSmsAccountAware(): SmsAccountAwareInterface;

    public function __construct(SmsContactInterface $smsContact, SmsLogParams $smsLogParams)
    {
        $this->smsContact   = $smsContact;
        $this->smsLogParams = $smsLogParams;
    }

    public function getSmsContact(): SmsContactInterface
    {
        return $this->smsContact;
    }

    public function getSmsLogParams(): SmsLogParams
    {
        return $this->smsLogParams;
    }
}
