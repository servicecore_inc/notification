<?php

namespace ServiceCore\Notification\Core\Data;

abstract class Notification
{
    abstract public function getType(): string;
}
