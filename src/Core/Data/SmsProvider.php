<?php

namespace ServiceCore\Notification\Core\Data;

class SmsProvider
{
    public const PROVIDER_TWILIO   = 'twilio';
    public const PROVIDER_SNS      = 'sns';
    public const PROVIDER_OVERRIDE = 'override';
}
