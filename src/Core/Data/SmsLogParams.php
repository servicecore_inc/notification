<?php

namespace ServiceCore\Notification\Core\Data;

class SmsLogParams
{
    public string $destinationType;
    public int $destinationId;
    public string $sourceType;
    public int $sourceId;
    public string $status;
    public ?int $senderId;

    public function __construct(
        string $destinationType,
        int $destinationId,
        string $sourceType,
        int $sourceId,
        string $status,
        ?int $senderId = null
    ) {
        $this->destinationType = $destinationType;
        $this->destinationId   = $destinationId;
        $this->sourceType      = $sourceType;
        $this->sourceId        = $sourceId;
        $this->status          = $status;
        $this->senderId        = $senderId;
    }
}
