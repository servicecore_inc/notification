<?php

namespace ServiceCore\Notification\Core\ServiceManager\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use ServiceCore\Notification\Core\ServiceManager\SmsServiceManager;

class SmsServiceManagerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsServiceManager {
        $containerConfig = $container->get('config');
        $config          = $containerConfig['sms_service_manager'] ?? null;

        if ($config === null) {
            throw new \RuntimeException('Could not find sms service manager in config');
        }

        $config['services']['config'] = $containerConfig['notification'] ?? [];

        return new SmsServiceManager($config);
    }
}
