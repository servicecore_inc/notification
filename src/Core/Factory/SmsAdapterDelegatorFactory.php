<?php

namespace ServiceCore\Notification\Core\Factory;

use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;
use ServiceCore\Notification\Core\RoleData\SmsAdapterAwareInterface;
use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface as SmsServiceAdapterInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsAdapterInterface as SmsTransportAdapterInterface;

class SmsAdapterDelegatorFactory implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        ?array $options = null
    ): SmsAdapterAwareInterface {
        $class = $callback();

        if (!$class instanceof SmsAdapterAwareInterface) {
            throw new RuntimeException(
                \sprintf(
                    'Class %s does not implement %s',
                    \get_class($class),
                    SmsAdapterAwareInterface::class
                )
            );
        }

        $config  = $container->get('config')['notification'];
        $adapter = $config['sms']['adapter'] ?? null;

        if ($adapter === null) {
            throw new RuntimeException('Sms adapter could not be found.');
        }

        $smsService = $config[$adapter]['service'];

        if (!$container->has($smsService)) {
            throw new RuntimeException(\sprintf('Class %s not found in container', $smsService));
        }

        $smsServiceInstance = $container->get($smsService);

        if (!$smsServiceInstance instanceof SmsServiceAdapterInterface) {
            throw new RuntimeException(
                \sprintf(
                    'Sms adapter must implement %s',
                    SmsServiceAdapterInterface::class
                )
            );
        }

        $smsTransport = $config[$adapter]['transport'];

        if (!$container->has($smsTransport)) {
            throw new RuntimeException(\sprintf('Class %s not found in container', $smsTransport));
        }

        $smsTransportInstance = $container->get($smsTransport);

        if (!$smsTransportInstance instanceof SmsTransportAdapterInterface) {
            throw new RuntimeException(
                \sprintf(
                    'Sms adapter must implement %s',
                    SmsTransportAdapterInterface::class
                )
            );
        }

        $class->setSmsServiceAdapter($smsServiceInstance);
        $class->setSmsTransportAdapter($smsTransportInstance);

        return $class;
    }
}
