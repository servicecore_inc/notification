<?php

namespace ServiceCore\Notification\Core\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;
use Twilio\Http\CurlClient;
use Twilio\Rest\Client;

class TwilioRestClientFactory implements FactoryInterface
{
    private const DEFAULT_CLIENT_TIMEOUT = 10;

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): Client
    {
        $username = $options['accountId'] ?? null;
        $password = $options['accountToken'] ?? null;

        if ($username === null) {
            $username = $container->get('config')['twilio']['sid'] ?? null;

            if ($username === null) {
                throw new RuntimeException('Could not find account ID');
            }
        }

        if ($password === null) {
            $password = $container->get('config')['twilio']['token'] ?? null;

            if ($password === null) {
                throw new RuntimeException('Could not find account Token');
            }
        }

        $timeout    = $container->get('config')['twilio']['timeout'] ?? self::DEFAULT_CLIENT_TIMEOUT;
        $httpClient = new CurlClient([\CURLOPT_TIMEOUT => $timeout]);

        return new Client(
            $username,
            $password,
            null,
            null,
            $httpClient
        );
    }
}
