<?php

namespace ServiceCore\Notification\Services\Core\Exception;

use Exception;
use Throwable;

class SmsCampaignRegisterException extends Exception
{
    public const CREATE_A2P_CAMPAIGN_ERROR = 'Failed to Register A2P Campaign: ';

    public function __construct(string $message, $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(
            \sprintf(
                '%s %s',
                self::CREATE_A2P_CAMPAIGN_ERROR,
                $message
            ),
            $code,
            $previous
        );
    }
}
