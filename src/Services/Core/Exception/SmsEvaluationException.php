<?php

namespace ServiceCore\Notification\Services\Core\Exception;

use Exception;
use Throwable;

class SmsEvaluationException extends Exception
{
    public const EVALUATION_ERROR = 'An error occurred while attempting to evaluate ';

    public function __construct(
        string $message,
        string $entityName = '',
        $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct(
            \sprintf(
                '%s %s %s',
                self::EVALUATION_ERROR,
                $entityName,
                $message
            ),
            $code,
            $previous
        );
    }
}
