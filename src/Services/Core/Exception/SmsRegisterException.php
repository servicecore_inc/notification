<?php

namespace ServiceCore\Notification\Services\Core\Exception;

use Exception;
use Throwable;

class SmsRegisterException extends Exception
{
    public const CREATE_SUB_ACCOUNT_ERROR = 'An error occurred while attempting to register for SMS: ';

    public function __construct(string $message, $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(
            \sprintf(
                '%s %s',
                self::CREATE_SUB_ACCOUNT_ERROR,
                $message
            ),
            $code,
            $previous
        );
    }
}
