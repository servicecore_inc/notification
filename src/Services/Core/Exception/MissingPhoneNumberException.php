<?php

namespace ServiceCore\Notification\Services\Core\Exception;

use Exception;
use Throwable;

final class MissingPhoneNumberException extends Exception
{
    public const COULD_NOT_FIND_NUMBER = 'Unable to acquire a phone number';

    public function __construct(string $message = '', $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(
            \sprintf(
                '%s %s',
                self::COULD_NOT_FIND_NUMBER,
                $message
            ),
            $code,
            $previous
        );
    }
}
