<?php

namespace ServiceCore\Notification\Services\SmsService\Adapter;

use ServiceCore\Notification\Core\Data\SmsProvider;
use ServiceCore\Notification\Services\SmsService\Data\SmsBrand;
use ServiceCore\Notification\Services\SmsService\Data\SmsCampaign;
use ServiceCore\Notification\Services\SmsService\Data\SmsSubAccount;
use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface;

class SmsSnsAdapter implements SmsAdapterInterface
{
    /**
     * @TODO Implement this method
     */
    public function registerBrand(array $properties): SmsBrand
    {
        return new SmsBrand('sns', '', $properties);
    }

    /**
     * @TODO Implement this method
     */
    public function createCampaign(array $properties): SmsCampaign
    {
        return new SmsCampaign('sns', $properties);
    }

    /**
     * @TODO Implement this method
     */
    public function isValidWebhookRequest(array $data): bool
    {
        return $data['isValid'] ?? false;
    }

    /**
     * @TODO Implement this method
     */
    public function fetchSubAccount(string $sid): SmsSubAccount
    {
        return new SmsSubAccount('sns', 'sns');
    }

    public function getAdapterName(): string
    {
        return SmsProvider::PROVIDER_SNS;
    }
}
