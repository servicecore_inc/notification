<?php

namespace ServiceCore\Notification\Services\SmsService\Adapter\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsOverrideAdapter;

class SmsOverrideAdapterFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsOverrideAdapter {
        return new SmsOverrideAdapter();
    }
}
