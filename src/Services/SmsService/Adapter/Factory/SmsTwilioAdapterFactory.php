<?php

namespace ServiceCore\Notification\Services\SmsService\Adapter\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use ServiceCore\Notification\Core\ServiceManager\SmsServiceManager;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsTwilioAdapter;

class SmsTwilioAdapterFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsTwilioAdapter {
        /** @var SmsServiceManager $smsContainer */
        $smsContainer = $container->get(SmsServiceManager::class);

        return new SmsTwilioAdapter($smsContainer);
    }
}
