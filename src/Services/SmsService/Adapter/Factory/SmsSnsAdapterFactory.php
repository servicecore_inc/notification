<?php

namespace ServiceCore\Notification\Services\SmsService\Adapter\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use ServiceCore\Notification\Services\SmsService\Adapter\SmsSnsAdapter;

class SmsSnsAdapterFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsSnsAdapter {
        return new SmsSnsAdapter();
    }
}
