<?php

namespace ServiceCore\Notification\Services\SmsService\Adapter;

use Laminas\ServiceManager\ServiceLocatorInterface;
use ServiceCore\Notification\Core\Data\SmsProvider;
use ServiceCore\Notification\Core\Exception\SmsException;
use ServiceCore\Notification\Services\Core\Exception\MissingPhoneNumberException;
use ServiceCore\Notification\Services\Core\Exception\SmsCampaignRegisterException;
use ServiceCore\Notification\Services\Core\Exception\SmsEvaluationException;
use ServiceCore\Notification\Services\Core\Exception\SmsRegisterException;
use ServiceCore\Notification\Services\SmsService\Data\SmsBrand;
use ServiceCore\Notification\Services\SmsService\Data\SmsCampaign;
use ServiceCore\Notification\Services\SmsService\Data\SmsSubAccount;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessIdentity;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessIndustry;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioBusinessType;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioCompanyType;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioEventSinkType;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioRegion;
use ServiceCore\Notification\Services\SmsService\Data\SmsTwilioUseCase;
use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Api\V2010\Account\AddressInstance;
use Twilio\Rest\Api\V2010\Account\IncomingPhoneNumberInstance;
use Twilio\Rest\Api\V2010\AccountInstance;
use Twilio\Rest\Client as RestClient;
use Twilio\Rest\Messaging\V1\BrandRegistrationInstance;
use Twilio\Rest\Messaging\V1\Service\UsAppToPersonInstance;
use Twilio\Rest\Messaging\V1\ServiceInstance;
use Twilio\Rest\Studio\V2\FlowInstance;
use Twilio\Rest\Trusthub\V1\CustomerProfilesContext;
use Twilio\Rest\Trusthub\V1\CustomerProfilesInstance;
use Twilio\Rest\Trusthub\V1\EndUserInstance;
use Twilio\Rest\Trusthub\V1\SupportingDocumentInstance;
use Twilio\Rest\Trusthub\V1\TrustProductsContext;
use Twilio\Rest\Trusthub\V1\TrustProductsInstance;
use Twilio\Security\RequestValidator;

class SmsTwilioAdapter implements SmsAdapterInterface
{
    public const SECONDARY_CUSTOMER_PROFILE_GLOBAL_POLICY_SID = 'RNdfbf3fae0e1107f8aded0e7cead80bf5';
    public const A2P_MESSAGING_TRUST_HUB_POLICY_SID           = 'RNb0d4771c2c98518d916a3d4cd70a8f8b';
    public const END_USER_CUSTOMER_PROFILE_BUSINESS_INFO_TYPE = 'customer_profile_business_information';
    public const END_USER_US_A2P_MESSAGING_PROFILE            = 'us_a2p_messaging_profile_information';
    public const AUTHORIZED_REPRESENTATIVE_TYPE               = 'authorized_representative_1';
    public const ADDRESS_DOCUMENTS_TYPE                       = 'customer_profile_address';
    public const EVALUATION_COMPLIANT                         = 'compliant';
    public const REGISTRATION_SINK                            = 'Registration Sink';
    public const REGISTRATION_SUBSCRIPTION                    = 'Registration Subscription';

    private const DEFAULT_COUNTRY_CODE = 'US';

    private RestClient $restClient;

    private ServiceLocatorInterface $container;

    public function __construct(ServiceLocatorInterface $container)
    {
        $this->container  = $container;
        $this->restClient = $this->container->build(RestClient::class);
    }

    public function registerBrand(array $properties): SmsBrand
    {
        $subAccount = $this->createSubAccount($properties['companyName']);
        $subClient  = $this->container->build(RestClient::class, [
            'accountId'    => $subAccount->sid,
            'accountToken' => $subAccount->authToken
        ]);

        $customerProfile = $this->createSecondaryProfile($subClient, $properties);
        $a2pProfile      = $this->createA2PTrustProduct($subClient, $customerProfile->sid, $properties);

        $this->registerWebhook($subClient, $properties['webhookUrl']);

        $brandRegistration = $this->createA2PBrand(
            $subClient,
            $customerProfile->sid,
            $a2pProfile->sid,
            $properties['skipAutomaticSecVet'] ?? false,
            $properties['isMock'] ?? false
        );

        return new SmsBrand(
            $brandRegistration->sid,
            $brandRegistration->accountSid,
            $brandRegistration->toArray()
        );
    }

    public function createCampaign(array $properties): SmsCampaign
    {
        $subAccount        = $this->fetchSubAccount($properties['accountSid']);
        $subClient         = $this->container->build(RestClient::class, [
            'accountId'    => $subAccount->sid,
            'accountToken' => $subAccount->authToken
        ]);
        $phoneNumberParams = [
            'areaCode'     => $properties['areaCode'] ?? null,
            'contains'     => $properties['contains'] ?? null,
            'smsEnabled'   => true,
            'inPostalCode' => $properties['inPostalCode'] ?? null,
            'inRegion'     => $properties['inRegion'] ?? null,
            'inLocality'   => $properties['inLocality'] ?? null,
        ];

        if (
            \array_key_exists('flowName', $properties)
            && \array_key_exists('flowDefinition', $properties)
            && $properties['flowName']
            && \is_array($properties['flowDefinition'])
            && \count($properties['flowDefinition']) > 0
        ) {
            $studioFlow = $this->createFlow(
                $subClient,
                $properties['flowName'],
                $properties['flowDefinition']
            );

            $phoneNumberParams['flowUrl'] = $studioFlow->webhookUrl;
        }

        $phone = $this->createPhoneNumber(
            $subClient,
            $properties['companyCountryCode'] ?? self::DEFAULT_COUNTRY_CODE,
            $phoneNumberParams
        );

        $messagingService = $this->createMessagingService(
            $subClient,
            $properties['companyName'],
            $properties['statusWebhookUrl']
        );

        $this->assignMessageServicePhone($subClient, $messagingService->sid, $phone->sid);

        $campaignRegistration = $this->createA2PCampaign($subClient, $messagingService->sid, $properties);

        return new SmsCampaign($campaignRegistration->sid, $campaignRegistration->toArray());
    }

    public function getAdapterName(): string
    {
        return SmsProvider::PROVIDER_TWILIO;
    }

    public function isValidWebhookRequest(array $data): bool
    {
        $subAccount = $this->fetchSubAccount($data['accountId']);

        return (new RequestValidator($subAccount->authToken))->validate(
            $data['signature'],
            $data['url'],
            $data['vars']
        );
    }

    public function fetchSubAccount(string $sid): SmsSubAccount
    {
        $accountInstance = $this->restClient->api->v2010->accounts($sid)->fetch();

        return new SmsSubAccount(
            $accountInstance->sid,
            $accountInstance->authToken,
            $accountInstance->toArray()
        );
    }

    private function createSubAccount(string $name): AccountInstance
    {
        return $this->restClient->api->v2010->accounts->create(['friendlyName' => $name]);
    }

    private function createSecondaryProfile(
        RestClient $subClient,
        array $properties
    ): CustomerProfilesInstance {
        $primaryBundleSid = $this->container->get('config')['twilio']['primaryBundleSid'] ?? null;

        if ($primaryBundleSid === null) {
            throw new SmsRegisterException('Primary bundle id missing');
        }

        $profileBundle = $this->createSecondaryProfileBundle(
            $subClient,
            $properties['companyName'],
            $properties['companyEmail'],
            $properties['webhookUrl'],
        );

        $businessInfoResponse     = $this->createCustomerProfileBusinessInfo($subClient, $properties);
        $authorizedRepresentative = $this->createAuthorizedRepresentative($subClient, $properties);
        $profileAddress           = $this->createCustomerAddress($subClient, $properties);

        $addressDocument = $this->createCustomerAddressDocument(
            $subClient,
            $properties['companyName'],
            $profileAddress->sid
        );

        $customerProfile = $this->getCustomerProfile($subClient, $profileBundle->sid);

        $this->assignCustomerProfileEntity(
            $customerProfile,
            $primaryBundleSid,
            $businessInfoResponse->sid,
            $authorizedRepresentative->sid,
            $addressDocument->sid
        );

        $this->evaluateCustomerProfiles($customerProfile);

        return $this->submitCustomerProfileReview($customerProfile);
    }

    private function createSecondaryProfileBundle(
        RestClient $subClient,
        string $name,
        string $email,
        string $callbackUrl
    ): CustomerProfilesInstance {
        try {
            return $subClient->trusthub->v1->customerProfiles->create(
                $name,
                $email,
                self::SECONDARY_CUSTOMER_PROFILE_GLOBAL_POLICY_SID,
                [
                    'statusCallback' => $callbackUrl
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createCustomerProfileBusinessInfo(
        RestClient $subClient,
        array $properties
    ): EndUserInstance {
        if (!\in_array($properties['regions'], SmsTwilioRegion::$smsTwilioRegions, true)) {
            throw new SmsRegisterException(
                $properties['regions'] . ' is not a valid region'
            );
        }

        if (
            !\in_array(
                $properties['businessType'],
                SmsTwilioBusinessType::$smsTwilioBusinessTypes,
                true
            )
        ) {
            throw new SmsRegisterException(
                $properties['businessType'] . ' is not a valid business type'
            );
        }

        if (
            !\in_array(
                $properties['businessIndustry'],
                SmsTwilioBusinessIndustry::$businessIndustries,
                true
            )
        ) {
            throw new SmsRegisterException(
                $properties['businessIndustry'] . ' is not a valid business industry'
            );
        }

        if (
            !\in_array(
                $properties['businessIdentity'],
                SmsTwilioBusinessIdentity::$smsTwilioBusinessIdentities,
                true
            )
        ) {
            throw new SmsRegisterException(
                $properties['businessIdentity'] . ' is not a valid business identity'
            );
        }

        try {
            return $subClient->trusthub->v1->endUsers->create(
                $properties['companyName'],
                self::END_USER_CUSTOMER_PROFILE_BUSINESS_INFO_TYPE,
                [
                    'attributes' => [
                        'business_name'                    => $properties['companyName'],
                        'social_media_profile_urls'        => $properties['socialMediaTag'] ?? null,
                        'website_url'                      => $properties['website'],
                        'business_regions_of_operation'    => $properties['regions'],
                        'business_type'                    => $properties['businessType'],
                        'business_registration_identifier' => $properties['businessRegistrationIdentifier'],
                        'business_registration_number'     => $properties['businessRegistrationNumber'],
                        'business_identity'                => $properties['businessIdentity'],
                        'business_industry'                => $properties['businessIndustry'],
                    ]
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createAuthorizedRepresentative(
        RestClient $subClient,
        array $properties
    ): EndUserInstance {
        try {
            return $subClient->trusthub->v1->endUsers->create(
                $properties['ownerName'],
                self::AUTHORIZED_REPRESENTATIVE_TYPE,
                [
                    'attributes' => [
                        'first_name'     => $properties['ownerFirstName'],
                        'last_name'      => $properties['ownerLastName'],
                        'phone_number'   => $properties['ownerPhone'],
                        'email'          => $properties['ownerEmail'],
                        'job_position'   => $properties['ownerPosition'],
                        'business_title' => $properties['ownerPosition'],
                    ]
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createCustomerAddress(
        RestClient $subClient,
        array $properties
    ): AddressInstance {
        try {
            return $subClient->addresses
                ->create(
                    $properties['companyName'],
                    $properties['companyAddress'],
                    $properties['companyCity'],
                    $properties['companyState'],
                    $properties['companyZipCode'],
                    $properties['companyCountry'],
                    [
                        'friendlyName'    => $properties['companyName'],
                        'streetSecondary' => $properties['companySecondaryAddress']
                    ],
                );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createCustomerAddressDocument(
        RestClient $subClient,
        string $name,
        string $sid
    ): SupportingDocumentInstance {
        return $subClient->trusthub->v1->supportingDocuments->create(
            $name,
            $this::ADDRESS_DOCUMENTS_TYPE,
            [
                'attributes' => [
                    'address_sids' => $sid
                ]
            ]
        );
    }

    private function getCustomerProfile(
        RestClient $subClient,
        string $sid
    ): CustomerProfilesContext {
        try {
            return $subClient->trusthub->v1->customerProfiles($sid);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function assignCustomerProfileEntity(
        CustomerProfilesContext $customerProfile,
        string $profileBundleSid,
        string $businessInfoResponseSid,
        string $authorizedRepresentativeSid,
        string $profileAddressSid
    ): void {
        try {
            $customerProfile->customerProfilesEntityAssignments->create($profileBundleSid);
            $customerProfile->customerProfilesEntityAssignments->create($businessInfoResponseSid);
            $customerProfile->customerProfilesEntityAssignments->create($authorizedRepresentativeSid);
            $customerProfile->customerProfilesEntityAssignments->create($profileAddressSid);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function evaluateCustomerProfiles(
        CustomerProfilesContext $customerProfile
    ): void {
        try {
            $evaluation = $customerProfile->customerProfilesEvaluations->create(
                self::SECONDARY_CUSTOMER_PROFILE_GLOBAL_POLICY_SID
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }

        if ($evaluation->status !== self::EVALUATION_COMPLIANT) {
            $merged   = \array_merge_recursive(...$evaluation->results)['fields'];
            $filtered = \array_filter($merged, static function ($field) {
                return $field['failure_reason'] !== null;
            });

            $reduced = \array_reduce($filtered, static function ($concatString, $value) {
                return $concatString . $value['failure_reason'];
            }, '');

            throw new SmsEvaluationException(
                $reduced,
                CustomerProfilesContext::class
            );
        }
    }

    private function submitCustomerProfileReview(
        CustomerProfilesContext $customerProfile
    ): CustomerProfilesInstance {
        try {
            return $customerProfile->update(['status' => 'pending-review']);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createA2PTrustProduct(
        RestClient $subClient,
        string $customerProfileSid,
        array $properties
    ): TrustProductsInstance {
        $trustProductBundle = $this->createTrustProductBundle(
            $subClient,
            $properties['companyName'],
            $properties['companyEmail'],
            $properties['webhookUrl'],
        );

        $endUser = $this->createCompanyMessagingProfile(
            $subClient,
            $properties['companyName'],
            $properties['companyType']
        );

        $trustProductContext = $this->createTrustProduct($subClient, $trustProductBundle->sid);

        $this->assignTrustProductEntity(
            $trustProductContext,
            $endUser->sid,
            $customerProfileSid
        );

        $this->evaluateTrustProduct($trustProductContext);

        return $this->submitTrustBundleReview($trustProductContext);
    }

    private function createTrustProductBundle(
        RestClient $subClient,
        string $name,
        string $email,
        string $callbackUrl
    ): TrustProductsInstance {
        try {
            return $subClient->trusthub->v1->trustProducts->create(
                $name,
                $email,
                self::A2P_MESSAGING_TRUST_HUB_POLICY_SID,
                [
                    'statusCallback' => $callbackUrl
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createCompanyMessagingProfile(
        RestClient $subClient,
        string $name,
        string $type,
        ?string $stockExchange = null,
        ?string $stockTicker = null
    ): EndUserInstance {
        $attributes = [
            'company_type' => $type
        ];

        if ($type === SmsTwilioCompanyType::PUBLIC) {
            $attributes['stock_exchange'] = $stockExchange;
            $attributes['stock_ticker']   = $stockTicker;
        }

        try {
            return $subClient->trusthub->v1->endUsers->create(
                $name,
                self::END_USER_US_A2P_MESSAGING_PROFILE,
                [
                    'attributes' => $attributes
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createTrustProduct(
        RestClient $subClient,
        string $sid
    ): TrustProductsContext {
        try {
            return $subClient->trusthub->v1->trustProducts($sid);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function assignTrustProductEntity(
        TrustProductsContext $trustProduct,
        $endUserSid,
        $customerProfileSid
    ): void {
        try {
            $trustProduct->trustProductsEntityAssignments->create($endUserSid);
            $trustProduct->trustProductsEntityAssignments->create($customerProfileSid);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function evaluateTrustProduct(
        TrustProductsContext $trustProductsContext
    ): void {
        try {
            $evaluation = $trustProductsContext->trustProductsEvaluations->create(
                self::A2P_MESSAGING_TRUST_HUB_POLICY_SID
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }

        if ($evaluation->status !== self::EVALUATION_COMPLIANT) {
            $merged   = \array_merge_recursive(...$evaluation->results)['fields'];
            $filtered = \array_filter($merged, static function ($field) {
                return $field['failure_reason'] !== null;
            });

            $reduced = \array_reduce($filtered, static function ($concatString, $value) {
                return $concatString . $value['failure_reason'];
            }, '');

            throw new SmsEvaluationException(
                $reduced,
                TrustProductsContext::class
            );
        }
    }

    private function submitTrustBundleReview(
        TrustProductsContext $trustProductContext
    ): TrustProductsInstance {
        try {
            return $trustProductContext->update(['status' => 'pending-review']);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function registerWebhook(RestClient $subClient, string $webhook): void
    {
        $sink = $subClient->events->v1->sinks->create(
            self::REGISTRATION_SINK,
            [
                'destination'  => $webhook,
                'method'       => 'POST',
                'batch_events' => false
            ],
            'webhook'
        );

        $subClient->events->v1->subscriptions->create(
            self::REGISTRATION_SUBSCRIPTION,
            $sink->sid,
            [
                [
                    'type' => SmsTwilioEventSinkType::BRAND_REGISTRATION_FAILURE
                ],
                [
                    'type' => SmsTwilioEventSinkType::BRAND_REGISTRATION_REGISTERED,
                ],
                [
                    'type' => SmsTwilioEventSinkType::BRAND_REGISTRATION_UNVERIFIED,
                ],
                [
                    'type' => SmsTwilioEventSinkType::CAMPAIGN_REGISTRATION_APPROVED,
                ],
                [
                    'type' => SmsTwilioEventSinkType::CAMPAIGN_REGISTRATION_SUBMITTED,
                ],
                [
                    'type' => SmsTwilioEventSinkType::CAMPAIGN_REGISTRATION_FAILURE,
                ]
            ]
        );
    }

    private function createA2PBrand(
        RestClient $subClient,
        string $customerProfileSid,
        string $a2pProfileSid,
        bool $skipAutomaticSecVet,
        bool $mock
    ): BrandRegistrationInstance {
        try {
            return $subClient->messaging->v1->brandRegistrations->create(
                $customerProfileSid,
                $a2pProfileSid,
                [
                    'skipAutomaticSecVet' => $skipAutomaticSecVet, // TODO: Look into for smaller companies
                    'mock'                => $mock
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createFlow(
        RestClient $subClient,
        string $name,
        array $flowDefinition
    ): FlowInstance {
        try {
            return $subClient->studio->v2->flows->create(
                $name,
                'published',
                $flowDefinition
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createPhoneNumber(
        RestClient $subClient,
        string $countryCode,
        ?array $options = []
    ): IncomingPhoneNumberInstance {
        try {
            $phoneNumberInstance = $subClient->availablePhoneNumbers(
                $countryCode
            )->local->read($options, 10)[0] ?? null;
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }

        if (!$phoneNumberInstance) {
            throw new MissingPhoneNumberException();
        }

        try {
            $phoneParam = [
                'phoneNumber' => $phoneNumberInstance->phoneNumber,
            ];

            if (\array_key_exists('flowUrl', $options) && $options['flowUrl'] !== null) {
                $phoneParam['smsUrl'] = $options['flowUrl'];
            }

            return $subClient->incomingPhoneNumbers->create($phoneParam);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createMessagingService(
        RestClient $subClient,
        string $companyName,
        ?string $statusWebhookUrl
    ): ServiceInstance {
        try {
            return $subClient->messaging->v1->services->create(
                $companyName,
                [
                    'statusCallback'            => $statusWebhookUrl,
                    'stickySender'              => true,
                    'UseInboundWebhookOnNumber' => true,
                ]
            );
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function assignMessageServicePhone(
        RestClient $subClient,
        string $serviceSid,
        string $phoneNumberSid
    ): void {
        try {
            $subClient->messaging->v1->services($serviceSid)->phoneNumbers->create($phoneNumberSid);
        } catch (RestException $e) {
            throw new SmsRegisterException(
                $e->getMessage(),
                $e->getCode()
            );
        } catch (TwilioException $e) {
            throw new SmsException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    private function createA2PCampaign(
        RestClient $subClient,
        string $messageServiceSid,
        array $properties
    ): UsAppToPersonInstance {
        if (!\in_array($properties['useCase'], SmsTwilioUseCase::$smsTwilioUseCases, true)) {
            throw new SmsCampaignRegisterException(
                $properties['useCase'] . ' is not a valid use case'
            );
        }

        if (
            !$properties['optInKeyWords'] &&
            !$properties['optInMessage'] &&
            !$properties['optOutKeyWords'] &&
            !$properties['optOutMessage']
        ) {
            throw new SmsCampaignRegisterException(
                'Must have optIn and/or optOut options.'
            );
        }

        return $subClient->messaging->v1->services($messageServiceSid)
            ->usAppToPerson
            ->create(
                $properties['brandRegistrationSid'],
                $properties['description'] ?? '',
                $properties['messageFlow'] ?? '',
                $properties['messageSamples'] ?? [],
                $properties['useCase'] ?? SmsTwilioUseCase::MIXED_USE_CASE,
                $properties['hasEmbeddedLinks'] ?? false,
                $properties['hasEmbeddedPhone'] ?? false,
                [
                    'optInKeywords'  => $properties['optInKeyWords'] ?? null,
                    'optInMessage'   => $properties['optInMessage'] ?? null,
                    'optOutKeywords' => $properties['optOutKeyWords'] ?? null,
                    'optOutMessage'  => $properties['optOutMessage'] ?? null
                ]
            );
    }
}
