<?php

namespace ServiceCore\Notification\Services\SmsService\Adapter;

use ServiceCore\Notification\Core\Data\SmsProvider;
use ServiceCore\Notification\Services\SmsService\Data\SmsBrand;
use ServiceCore\Notification\Services\SmsService\Data\SmsCampaign;
use ServiceCore\Notification\Services\SmsService\Data\SmsSubAccount;
use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface;

class SmsOverrideAdapter implements SmsAdapterInterface
{
    public function registerBrand(array $properties): SmsBrand
    {
        return new SmsBrand('foo', 'bar', $properties);
    }

    public function createCampaign(array $properties): SmsCampaign
    {
        return new SmsCampaign('foo', $properties);
    }

    public function getAdapterName(): string
    {
        return SmsProvider::PROVIDER_OVERRIDE;
    }

    public function isValidWebhookRequest(array $data): bool
    {
        return $data['isValid'] ?? true;
    }

    public function fetchSubAccount(string $sid): SmsSubAccount
    {
        return new SmsSubAccount('foo', 'bar');
    }
}
