<?php

namespace ServiceCore\Notification\Services\SmsService\Role;

use ServiceCore\Notification\Services\SmsService\RoleData\SmsAdapterInterface;

class Account
{
    private SmsAdapterInterface $smsAdapter;

    public function __construct(SmsAdapterInterface $smsAdapter)
    {
        $this->smsAdapter = $smsAdapter;
    }

    public function createCampaign(array $properties): void
    {
        $this->smsAdapter->createCampaign($properties);
    }

    public function registerBrand(): void
    {
        $brandRegistrationProperties = [];

        $this->smsAdapter->registerBrand($brandRegistrationProperties);
    }
}
