<?php

namespace ServiceCore\Notification\Services\SmsService\RoleData;

use ServiceCore\Notification\Services\SmsService\Data\SmsBrand;
use ServiceCore\Notification\Services\SmsService\Data\SmsCampaign;
use ServiceCore\Notification\Services\SmsService\Data\SmsSubAccount;
use ServiceCore\Notification\Transports\Core\RoleData\SmsAdapterInterface as BaseSmsAdapterInterface;

interface SmsAdapterInterface extends BaseSmsAdapterInterface
{
    public function registerBrand(array $properties): SmsBrand;

    public function createCampaign(array $properties): SmsCampaign;

    public function fetchSubAccount(string $sid): SmsSubAccount;

    public function isValidWebhookRequest(array $data): bool;
}
