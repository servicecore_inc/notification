<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsBrand
{
    public string $sid;
    public string $accountSid;
    private array $properties = [];

    public function __construct(
        string $sid,
        string $accountSid,
        array $properties = []
    ) {
        $this->sid        = $sid;
        $this->accountSid = $accountSid;
        $this->properties = \array_merge($this->properties, $properties);
    }

    public function __get(string $key)
    {
        if (\array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        }

        throw new \InvalidArgumentException("Property ${$key} does not exist.");
    }

    public function __set(string $key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function __isset(string $key): bool
    {
        return \array_key_exists($key, $this->properties);
    }
}
