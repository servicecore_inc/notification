<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioBusinessType
{
    public const PARTNERSHIP                   = 'Partnership';
    public const LIMITED_LIABILITY_CORPORATION = 'Limited Liability Corporation';
    public const CO_OPERATIVE                  = 'Co-operative';
    public const NON_PROFIT_CORPORATION        = 'Non-profit Corporation';
    public const CORPORATION                   = 'Corporation';

    public static array $smsTwilioBusinessTypes = [
        self::PARTNERSHIP,
        self::LIMITED_LIABILITY_CORPORATION,
        self::CO_OPERATIVE,
        self::NON_PROFIT_CORPORATION,
        self::CORPORATION,
    ];
}
