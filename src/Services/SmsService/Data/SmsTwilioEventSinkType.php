<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioEventSinkType
{
    public const BRAND_REGISTRATION_FAILURE = 'com.twilio.messaging.compliance.brand-registration.brand-failure';

    public const BRAND_REGISTRATION_UNVERIFIED
        = 'com.twilio.messaging.compliance.brand-registration.brand-unverified';

    public const BRAND_REGISTRATION_REGISTERED
        = 'com.twilio.messaging.compliance.brand-registration.brand-registered';

    public const CAMPAIGN_REGISTRATION_SUBMITTED
        = 'com.twilio.messaging.compliance.campaign-registration.campaign-submitted';

    public const CAMPAIGN_REGISTRATION_FAILURE
        = 'com.twilio.messaging.compliance.campaign-registration.campaign-failure';

    public const CAMPAIGN_REGISTRATION_APPROVED
        = 'com.twilio.messaging.compliance.campaign-registration.campaign-approved';
}
