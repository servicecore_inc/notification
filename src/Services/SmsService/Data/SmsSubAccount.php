<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsSubAccount
{
    public string $sid;
    public string $authToken;
    private array $properties = [];

    public function __construct(
        string $sid,
        string $authToken,
        array $properties = []
    ) {
        $this->sid        = $sid;
        $this->authToken  = $authToken;
        $this->properties = \array_merge($this->properties, $properties);
    }

    public function __get(string $key)
    {
        if (\array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        }

        throw new \InvalidArgumentException("Property ${$key} does not exist.");
    }

    public function __set(string $key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function __isset(string $key): bool
    {
        return \array_key_exists($key, $this->properties);
    }
}
