<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioBusinessRegistrationIdentity
{
    public const EIN   = 'EIN';
    public const DUNS  = 'DUNS';
    public const CBN   = 'CBN';
    public const CN    = 'CN';
    public const ACN   = 'ACN';
    public const CIN   = 'CIN';
    public const VAT   = 'VAT';
    public const VATRN = 'VATRN';
    public const RN    = 'RN';
    public const OTHER = 'Other';

    public static array $smsTwilioBusinessRegistrationIdentityTypes = [
        self::EIN,
        self::DUNS,
        self::CBN,
        self::CN,
        self::ACN,
        self::CIN,
        self::VAT,
        self::VATRN,
        self::RN,
        self::OTHER,
    ];
}
