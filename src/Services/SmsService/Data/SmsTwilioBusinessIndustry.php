<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioBusinessIndustry
{
    public const AUTOMOTIVE                 = 'AUTOMOTIVE';
    public const AGRICULTURE                = 'AGRICULTURE';
    public const BANKING                    = 'BANKING';
    public const CONSTRUCTION               = 'CONSTRUCTION';
    public const CONSUMER                   = 'CONSUMER';
    public const EDUCATION                  = 'EDUCATION';
    public const ENGINEERING                = 'ENGINEERING';
    public const ENERGY                     = 'ENERGY';
    public const OIL_AND_GAS                = 'OIL_AND_GAS';
    public const FAST_MOVING_CONSUMER_GOODS = 'FAST_MOVING_CONSUMER_GOODS';
    public const FINANCIAL                  = 'FINANCIAL';
    public const FINTECH                    = 'FINTECH';
    public const FOOD_AND_BEVERAGE          = 'FOOD_AND_BEVERAGE';
    public const GOVERNMENT                 = 'GOVERNMENT';
    public const HEALTHCARE                 = 'HEALTHCARE';
    public const HOSPITALITY                = 'HOSPITALITY';
    public const INSURANCE                  = 'INSURANCE';
    public const LEGAL                      = 'LEGAL';
    public const MANUFACTURING              = 'MANUFACTURING';
    public const MEDIA                      = 'MEDIA';
    public const ONLINE                     = 'ONLINE';
    public const PROFESSIONAL_SERVICES      = 'PROFESSIONAL SERVICES';
    public const RAW_MATERIALS              = 'RAW_MATERIALS';
    public const REAL_ESTATE                = 'REAL_ESTATE';
    public const RELIGION                   = 'RELIGION';
    public const RETAIL                     = 'RETAIL';
    public const JEWELRY                    = 'JEWELRY';
    public const TECHNOLOGY                 = 'TECHNOLOGY';
    public const TELECOMMUNICATIONS         = 'TELECOMMUNICATIONS';
    public const TRANSPORTATION             = 'TRANSPORTATION';
    public const TRAVEL                     = 'TRAVEL';
    public const ELECTRONICS                = 'ELECTRONICS';
    public const NOT_FOR_PROFIT             = 'NOT_FOR_PROFIT';

    public static array $businessIndustries = [
        self::AUTOMOTIVE,
        self::AGRICULTURE,
        self::BANKING,
        self::CONSTRUCTION,
        self::CONSUMER,
        self::EDUCATION,
        self::ENGINEERING,
        self::ENERGY,
        self::OIL_AND_GAS,
        self::FAST_MOVING_CONSUMER_GOODS,
        self::FINANCIAL,
        self::FINTECH,
        self::FOOD_AND_BEVERAGE,
        self::GOVERNMENT,
        self::HEALTHCARE,
        self::HOSPITALITY,
        self::INSURANCE,
        self::LEGAL,
        self::MANUFACTURING,
        self::MEDIA,
        self::ONLINE,
        self::PROFESSIONAL_SERVICES,
        self::RAW_MATERIALS,
        self::REAL_ESTATE,
        self::RELIGION,
        self::RETAIL,
        self::JEWELRY,
        self::TECHNOLOGY,
        self::TELECOMMUNICATIONS,
        self::TRANSPORTATION,
        self::TRAVEL,
        self::ELECTRONICS,
        self::NOT_FOR_PROFIT,
    ];
}
