<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioRegion
{
    public const AFRICA         = 'AFRICA';
    public const ASIA           = 'ASIA';
    public const EUROPE         = 'EUROPE';
    public const LATIN_AMERICA  = 'LATIN_AMERICA';
    public const USA_AND_CANADA = 'USA_AND_CANADA';

    public static array $smsTwilioRegions = [
        self::AFRICA,
        self::ASIA,
        self::EUROPE,
        self::LATIN_AMERICA,
        self::USA_AND_CANADA,
    ];
}
