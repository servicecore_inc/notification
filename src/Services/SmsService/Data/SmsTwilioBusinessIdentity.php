<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioBusinessIdentity
{
    public const DIRECT_CUSTOMER         = 'direct_customer';
    public const ISV_RESELLER_OR_PARTNER = 'isv_reseller_or_partner';
    public const UNKNOWN                 = 'unknown';

    public static array $smsTwilioBusinessIdentities = [
        self::DIRECT_CUSTOMER,
        self::ISV_RESELLER_OR_PARTNER,
        self::UNKNOWN,
    ];
}
