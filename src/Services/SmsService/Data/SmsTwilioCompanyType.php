<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioCompanyType
{
    public const PUBLIC     = 'public';
    public const PRIVATE    = 'private';
    public const NON_PROFIT = 'non-profit';
    public const GOVERNMENT = 'government';

    public static array $smsTwilioCompanyTypes = [
        self::PUBLIC,
        self::PRIVATE,
        self::NON_PROFIT,
        self::GOVERNMENT,
    ];
}
