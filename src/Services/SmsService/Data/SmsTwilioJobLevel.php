<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioJobLevel
{
    public const DIRECTOR        = 'Director';
    public const GM              = 'GM';
    public const VP              = 'VP';
    public const CEO             = 'CEO';
    public const CFO             = 'CFO';
    public const GENERAL_COUNSEL = 'GeneralCounsel';
    public const OTHER           = 'Other';

    public static array $SmsTwilioJobLevels = [
        self::DIRECTOR,
        self::GM,
        self::VP,
        self::CEO,
        self::CFO,
        self::GENERAL_COUNSEL,
        self::OTHER,
    ];
}
