<?php

namespace ServiceCore\Notification\Services\SmsService\Data;

class SmsTwilioUseCase
{
    public const TWO_FACTOR_AUTHENTICATION_USE_CASE   = '2FA';
    public const ACCOUNT_NOTIFICATION_USE_CASE        = 'ACCOUNT_NOTIFICATION';
    public const AGENTS_AND_FRANCHISES_USE_CASE       = 'AGENTS_FRANCHISES';
    public const CHARITY_USE_CASE                     = 'CHARITY';
    public const PROXY_USE_CASE                       = 'PROXY';
    public const CUSTOMER_CARE_USE_CASE               = 'CUSTOMER_CARE';
    public const DELIVERY_NOTIFICATION_USE_CASE       = 'DELIVERY_NOTIFICATION';
    public const FRAUD_ALERT_USE_CASE                 = 'FRAUD_ALERT';
    public const HIGHER_EDUCATION_USE_CASE            = 'HIGHER_EDUCATION';
    public const MARKETING_USE_CASE                   = 'MARKETING';
    public const MIXED_USE_CASE                       = 'MIXED';
    public const POLLING_VOTING_USE_CASE              = 'POLLING_VOTING';
    public const PUBLIC_SERVICE_ANNOUNCEMENT_USE_CASE = 'PUBLIC_SERVICE_ANNOUNCEMENT';
    public const SECURITY_ALERT_USE_CASE              = 'SECURITY_ALERT';

    public static array $smsTwilioUseCases = [
        self::TWO_FACTOR_AUTHENTICATION_USE_CASE,
        self::ACCOUNT_NOTIFICATION_USE_CASE,
        self::AGENTS_AND_FRANCHISES_USE_CASE,
        self::CHARITY_USE_CASE,
        self::PROXY_USE_CASE,
        self::CUSTOMER_CARE_USE_CASE,
        self::DELIVERY_NOTIFICATION_USE_CASE,
        self::FRAUD_ALERT_USE_CASE,
        self::HIGHER_EDUCATION_USE_CASE,
        self::MARKETING_USE_CASE,
        self::MIXED_USE_CASE,
        self::POLLING_VOTING_USE_CASE,
        self::PUBLIC_SERVICE_ANNOUNCEMENT_USE_CASE,
        self::SECURITY_ALERT_USE_CASE,
    ];
}
