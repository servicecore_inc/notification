<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter;

use Laminas\ServiceManager\ServiceLocatorInterface;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use ServiceCore\Notification\Core\Data\SmsProvider;
use ServiceCore\Notification\Services\SmsService\Data\SmsSubAccount;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client as RestClient;

class SmsTwilioAdapter extends AbstractSmsAdapter
{
    private ServiceLocatorInterface $container;

    public function __construct(ServiceLocatorInterface $container, PhoneNumberUtil $phoneUtil)
    {
        $this->container = $container;

        parent::__construct($phoneUtil);
    }

    public function sendSms(
        string $to,
        string $from,
        string $body,
        ?SmsSubAccount $smsSubAccount = null
    ): SmsMessageResult {
        $errorCode    = null;
        $errorMessage = null;
        $toPhone      = $this->formatPhoneNumber($to);
        $params       = [];

        if ($toPhone === null) {
            return $this->getInvalidPhoneFormatSmsResult($to);
        }

        try {
            $fromPhone = $this->formatPhoneNumber($from);
        } catch (NumberParseException $e) {
            $fromPhone = $from;
        }

        if ($smsSubAccount) {
            $params = [
                'accountId'    => $smsSubAccount->sid,
                'accountToken' => $smsSubAccount->authToken
            ];
        }

        /** @var RestClient $restClient */
        $restClient = $this->container->build(RestClient::class, $params);

        try {
            $response = $restClient->messages->create(
                $toPhone,
                [
                    'From' => $fromPhone,
                    'Body' => $body
                ]
            );

            $sid        = $response->sid;
            $properties = $response->toArray();
        } catch (TwilioException $twilioException) {
            $sid          = null;
            $errorCode    = $twilioException->getCode();
            $errorMessage = $twilioException->getMessage();
            $properties   = [];
        }

        return new SmsMessageResult($sid, $errorCode, $errorMessage, $properties);
    }

    public function getAdapterName(): string
    {
        return SmsProvider::PROVIDER_TWILIO;
    }
}
