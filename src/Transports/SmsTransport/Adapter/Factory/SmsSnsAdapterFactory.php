<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter\Factory;

use Aws\Sns\SnsClient;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsSnsAdapter;

class SmsSnsAdapterFactory
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsSnsAdapter {
        $client = new SnsClient();

        return new SmsSnsAdapter($client, PhoneNumberUtil::getInstance());
    }
}
