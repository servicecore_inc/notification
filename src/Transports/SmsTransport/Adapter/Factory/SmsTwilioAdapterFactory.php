<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use libphonenumber\PhoneNumberUtil;
use Psr\Container\ContainerInterface;
use ServiceCore\Notification\Core\ServiceManager\SmsServiceManager;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsTwilioAdapter;

class SmsTwilioAdapterFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsTwilioAdapter {
        /** @var SmsServiceManager $smsServiceManager */
        $smsServiceManager = $container->get(SmsServiceManager::class);

        return new SmsTwilioAdapter($smsServiceManager, PhoneNumberUtil::getInstance());
    }
}
