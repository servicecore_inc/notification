<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use libphonenumber\PhoneNumberUtil;
use Psr\Container\ContainerInterface;
use ServiceCore\Notification\Transports\SmsTransport\Adapter\SmsOverrideAdapter;

class SmsOverrideAdapterFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SmsOverrideAdapter {
        return new SmsOverrideAdapter(PhoneNumberUtil::getInstance());
    }
}
