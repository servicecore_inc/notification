<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter;

use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsAdapterInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;

abstract class AbstractSmsAdapter implements SmsAdapterInterface
{
    private PhoneNumberUtil $phoneUtil;

    public function __construct(PhoneNumberUtil $phoneUtil)
    {
        $this->phoneUtil = $phoneUtil;
    }

    protected function formatPhoneNumber(string $number): ?string
    {
        $phoneNumber = $this->phoneUtil->parse($number, 'en');

        if ($phoneNumber === null) {
            return null;
        }

        return $this->phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
    }

    protected function getInvalidPhoneFormatSmsResult(string $to): SmsMessageResult
    {
        return new SmsMessageResult(
            null,
            500,
            \sprintf('Could not parse phone number "%s"', $to)
        );
    }
}
