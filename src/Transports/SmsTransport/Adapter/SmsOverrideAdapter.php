<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter;

use ServiceCore\Notification\Core\Data\SmsProvider;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;

class SmsOverrideAdapter extends AbstractSmsAdapter
{
    public function sendSms(string $to, string $from, string $body): SmsMessageResult
    {
        return new SmsMessageResult(
            null,
            null,
            null,
            [
                'to'   => $to,
                'from' => $from,
                'body' => $body
            ]
        );
    }

    public function getAdapterName(): string
    {
        return SmsProvider::PROVIDER_OVERRIDE;
    }
}
