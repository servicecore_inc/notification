<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Adapter;

use Aws\Exception\AwsException;
use Aws\Sns\SnsClient;
use libphonenumber\PhoneNumberUtil;
use ServiceCore\Notification\Core\Data\SmsProvider;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;

class SmsSnsAdapter extends AbstractSmsAdapter
{
    private SnsClient $service;

    public function __construct(SnsClient $snsClient, PhoneNumberUtil $phoneUtil)
    {
        parent::__construct($phoneUtil);

        $this->service = $snsClient;
    }

    public function sendSms(string $to, string $from, string $body): SmsMessageResult
    {
        $errorCode    = null;
        $errorMessage = null;
        $toPhone      = $this->formatPhoneNumber($to);

        if ($toPhone === null) {
            return $this->getInvalidPhoneFormatSmsResult($to);
        }

        try {
            $response = $this->service->publish(
                [
                    'Message'     => $body,
                    'PhoneNumber' => $toPhone
                ]
            );

            $sid        = $response->get('MessageId');
            $properties = $response->toArray();
        } catch (AwsException $awsException) {
            $sid          = null;
            $errorCode    = $awsException->getCode();
            $errorMessage = $awsException->getMessage();
            $properties   = [];
        }

        return new SmsMessageResult($sid, $errorCode, $errorMessage, $properties);
    }

    public function getAdapterName(): string
    {
        return SmsProvider::PROVIDER_SNS;
    }
}
