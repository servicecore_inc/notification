<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Role;

use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use ServiceCore\Notification\Transports\Core\Role\Transporter;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsTransporterData;
use Twilio\Rest\Client as RestClient;

class SmsOverrideTransporter implements Transporter
{
    private SmsTransporterData $self;

    private RestClient $twilio;

    private PhoneNumberUtil $phoneUtil;

    private string $fromPhone;

    public function __construct(
        SmsTransporterData $self,
        RestClient $twilio,
        PhoneNumberUtil $phoneUtil,
        string $fromPhone
    ) {
        $this->self      = $self;
        $this->twilio    = $twilio;
        $this->phoneUtil = $phoneUtil;
        $this->fromPhone = $fromPhone;
    }

    public function transport(array $options = []): void
    {
        $this->twilio->messages->create(
            $this->formatPhoneNumber($this->self->getPhoneNumber()),
            [
                'From' => $this->formatPhoneNumber($this->fromPhone),
                'Body' => $this->self->getShortBody()
            ]
        );
    }

    private function formatPhoneNumber(string $number): string
    {
        $phoneNumber = $this->phoneUtil->parse($number, 'en');

        if ($phoneNumber === null) {
            return '';
        }

        return $this->phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
    }
}
