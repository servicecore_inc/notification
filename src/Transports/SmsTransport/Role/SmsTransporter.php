<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Role;

use ServiceCore\Notification\Transports\Core\Role\Transporter;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsAdapterInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsTransporterData;

class SmsTransporter implements Transporter
{
    private SmsTransporterData $self;
    private SmsAdapterInterface $smsService;
    private string $fromPhone;

    public function __construct(
        SmsTransporterData $self,
        SmsAdapterInterface $smsService,
        string $fromPhone
    ) {
        $this->self       = $self;
        $this->smsService = $smsService;
        $this->fromPhone  = $fromPhone;
    }

    public function transport(array $options = []): SmsMessageResult
    {
        return $this->smsService->sendSms(
            $this->self->getPhoneNumber(),
            $this->fromPhone,
            $this->self->getShortBody(),
            $options['smsSubAccount'] ?? null
        );
    }
}
