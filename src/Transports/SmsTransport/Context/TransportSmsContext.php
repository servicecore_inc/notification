<?php

namespace ServiceCore\Notification\Transports\SmsTransport\Context;

use ServiceCore\Notification\Transports\Core\Role\Transporter;
use ServiceCore\Notification\Transports\SmsTransport\Role\SmsTransporter;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsAdapterInterface;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsMessageResult;
use ServiceCore\Notification\Transports\SmsTransport\RoleData\SmsTransporterData;

class TransportSmsContext implements Transporter
{
    public const FLAG = 0b001;

    private SmsTransporter $smsData;

    public function __construct(
        SmsTransporterData $smsData,
        SmsAdapterInterface $smsService,
        string $fromPhone
    ) {
        $this->smsData = new SmsTransporter($smsData, $smsService, $fromPhone);
    }

    public function transport(array $options = []): SmsMessageResult
    {
        return $this->smsData->transport($options);
    }
}
