<?php

namespace ServiceCore\Notification\Transports\SmsTransport\RoleData;

interface SmsTransporterData
{
    public function getPhoneNumber(): string;

    public function getShortBody(): string;
}
