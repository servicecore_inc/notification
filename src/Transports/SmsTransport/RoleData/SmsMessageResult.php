<?php

namespace ServiceCore\Notification\Transports\SmsTransport\RoleData;

class SmsMessageResult
{
    public ?string $sid;
    public ?string $errorCode;
    public ?string $errorMessage;

    private array $properties = [];

    public function __construct(
        ?string $sid = null,
        ?string $errorCode = null,
        ?string $errorMessage = null,
        array $properties = []
    ) {
        $this->sid          = $sid;
        $this->errorCode    = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->properties   = \array_merge($this->properties, $properties);
    }

    public function __get(string $key)
    {
        if (\array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        }

        throw new \InvalidArgumentException("Property ${$key} does not exist.");
    }

    public function __set(string $key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function __isset(string $key): bool
    {
        return \array_key_exists($key, $this->properties);
    }
}
