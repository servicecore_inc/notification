<?php

namespace ServiceCore\Notification\Transports\SmsTransport\RoleData;

use ServiceCore\Notification\Transports\Core\RoleData\SmsAdapterInterface as BaseSmsAdapterInterface;

interface SmsAdapterInterface extends BaseSmsAdapterInterface
{
    public function sendSms(string $to, string $from, string $body): SmsMessageResult;
}
