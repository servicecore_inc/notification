<?php

namespace ServiceCore\Notification\Transports\Core\Helper;

use RuntimeException;
use ServiceCore\Notification\Transports\Core\Service\ContextSender;

trait ContextSenderAware
{
    private ?ContextSender $contextSender = null;

    public function getContextSender(): ContextSender
    {
        if (!$this->contextSender) {
            throw new RuntimeException(
                '`contextSender` is not set. Have you setup a delegator factory or called the setter?'
            );
        }

        return $this->contextSender;
    }

    public function setContextSender(ContextSender $contextSender): self
    {
        $this->contextSender = $contextSender;

        return $this;
    }
}
