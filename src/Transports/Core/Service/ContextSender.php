<?php

namespace ServiceCore\Notification\Transports\Core\Service;

use Laminas\EventManager\EventManagerInterface;
use ServiceCore\Notification\Core\Data\Notification;
use ServiceCore\Notification\Transports\Core\Context\TransportNotificationContext;

class ContextSender
{
    private EventManagerInterface $eventManager;
    private string $scriptsPath;
    private string $layoutsPath;

    public function __construct(
        EventManagerInterface $eventManager,
        string $scriptsPath,
        string $layoutsPath
    ) {
        $this->eventManager = $eventManager;
        $this->scriptsPath  = $scriptsPath;
        $this->layoutsPath  = $layoutsPath;
    }

    public function sendOneByTransportFlag(Notification $notification, int $flag): void
    {
        $context = $this->getTransportNotificationContext($notification);

        $context->notifyByTransportFlag(
            $flag,
            $this->scriptsPath,
            $this->layoutsPath
        );
    }

    private function getTransportNotificationContext(
        Notification $notification
    ): TransportNotificationContext {
        return new TransportNotificationContext($notification, $this->eventManager);
    }
}
