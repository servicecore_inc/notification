<?php

namespace ServiceCore\Notification\Transports\Core\Role;

interface Transporter
{
    /**
     * @param array $options
     *
     * @return mixed
     */
    public function transport(array $options = []);
}
