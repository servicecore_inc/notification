<?php

namespace ServiceCore\Notification\Transports\Core\Role;

use ServiceCore\Notification\Transports\Core\Service\ContextSender;

interface ContextSendable
{
    public function getContextSender(): ContextSender;

    public function setContextSender(ContextSender $contextSender);
}
