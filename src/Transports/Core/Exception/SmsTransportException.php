<?php

namespace ServiceCore\Notification\Services\Core\Exception;

use Exception;
use Throwable;

class SmsTransportException extends Exception
{
    public const SMS_TRANSPORT_ERROR = 'An error occured while sending an SMS: ';

    public function __construct(string $message, $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(\sprintf('%s %s', self::SMS_TRANSPORT_ERROR, $message), $code, $previous);
    }
}
