<?php

namespace ServiceCore\Notification\Transports\Core\RoleData;

interface SmsAdapterInterface
{
    public function getAdapterName(): string;
}
