<?php

namespace ServiceCore\Notification\Transports\Core\Context;

use JetBrains\PhpStorm\Deprecated;
use Laminas\EventManager\EventManagerInterface;
use ServiceCore\Notification\Core\Data\Notification;

class TransportNotificationContext
{
    private Notification $notification;
    private EventManagerInterface $eventManager;

    public function __construct(
        Notification $notification,
        EventManagerInterface $eventManager
    ) {
        $this->notification = $notification;
        $this->eventManager = $eventManager;
    }

    public function notifyByTransportFlag(int $transportFlag, string $scriptsPath, string $layoutsPath): void
    {
        $this->eventManager->trigger(
            'notification.transport',
            $this,
            [
                'notification'     => $this->notification,
                'scriptsPath'      => $scriptsPath,
                'layoutsPath'      => $layoutsPath,
                'notificationType' => $transportFlag
            ]
        );
    }

    #[Deprecated(replacement: '%class%->notifyByTransportFlag(%parameter0%, %parameter1%, %parameter2%)')]
    public function notifyOne(int $transportFlag, string $scriptsPath, string $layoutsPath): void
    {
        $this->notifyByTransportFlag(
            $transportFlag,
            $scriptsPath,
            $layoutsPath
        );
    }
}
