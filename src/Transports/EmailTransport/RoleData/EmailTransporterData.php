<?php

namespace ServiceCore\Notification\Transports\EmailTransport\RoleData;

use Laminas\Mail\Address\AddressInterface;
use Laminas\Mail\AddressList;
use Traversable;

interface EmailTransporterData
{
    /**
     * @return string|null
     */
    public function getSubject(): ?string;

    /**
     * @param $scriptsPath
     * @param $layoutsPath
     * @return string
     */
    public function getHtmlBody($scriptsPath, $layoutsPath): string;

    /**
     * @return string
     */
    public function getTextBody(): string;

    /**
     * @return string|AddressInterface|array|AddressList|Traversable
     */
    public function getToAddress(): array;

    /**
     * @return string|null
     */
    public function getFromAddress(): ?string;

    /**
     * @return string|null
     */
    public function getFromName(): ?string;

    /**
     * @return AttachmentProvider[]
     */
    public function getAttachments(): array;
}
