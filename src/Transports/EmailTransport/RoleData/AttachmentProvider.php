<?php

namespace ServiceCore\Notification\Transports\EmailTransport\RoleData;

/**
 * Interface AttachmentProvider
 *
 * @author  Jason Campbell <jason@servicecore.com>
 * @package ServiceCore\Notification\Transports\EmailTransport\RoleData
 */
interface AttachmentProvider
{
    /**
     * @return string
     */
    public function getFilePath();

    /**
     * @return string
     */
    public function getFileName();

    /**
     * @return string
     */
    public function getMimeType();

    /**
     * @return string
     */
    public function getEncoding();

    /**
     * @return string
     */
    public function getDisposition();
}
