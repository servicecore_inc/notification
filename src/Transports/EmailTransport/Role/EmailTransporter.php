<?php

namespace ServiceCore\Notification\Transports\EmailTransport\Role;

use Laminas\Mail\Exception\RuntimeException;
use Laminas\Mail\Message;
use Laminas\Mail\Transport\TransportInterface;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Mime;
use Laminas\Mime\Part as MimePart;
use ServiceCore\Notification\Transports\Core\Role\Transporter;
use ServiceCore\Notification\Transports\EmailTransport\Exception\AttachmentNotFoundException;
use ServiceCore\Notification\Transports\EmailTransport\Exception\FromDetailsRequiredException;
use ServiceCore\Notification\Transports\EmailTransport\Exception\ScriptsPathRequiredException;
use ServiceCore\Notification\Transports\EmailTransport\RoleData\AttachmentProvider;
use ServiceCore\Notification\Transports\EmailTransport\RoleData\EmailTransporterData;

class EmailTransporter implements Transporter
{
    /** @var EmailTransporterData */
    private $self;

    /** @var TransportInterface */
    private $mailTransport;

    /** @var string */
    private $subaccount;

    /**
     * @param EmailTransporterData $self
     * @param TransportInterface   $mailTransport
     * @param string               $subaccount
     */
    public function __construct(
        EmailTransporterData $self,
        TransportInterface $mailTransport,
        string $subaccount
    ) {
        $this->self          = $self;
        $this->mailTransport = $mailTransport;
        $this->subaccount    = $subaccount;
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public function transport(array $options = []): bool
    {
        $this->validateOptions($options);

        if (\count($this->self->getToAddress()) <= 0) {
            return false;
        }

        $body = new MimeMessage();

        $this->populateBody($body, $options);

        $email = $this->createMessage();

        $this->setFromAndReplyTo($email, $options);

        $this->setAttachments($body);

        $email->setBody($body);

        $this->populateHeaders($email, $options);

        try {
            $this->mailTransport->send($email);
        } catch (RuntimeException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array $options
     *
     * @return bool
     * @throws ScriptsPathRequiredException
     * @throws FromDetailsRequiredException
     */
    private function validateOptions(array $options): bool
    {
        if (!\array_key_exists('scriptsPath', $options) || !\array_key_exists('layoutsPath', $options)) {
            throw new ScriptsPathRequiredException();
        }

        if (!\array_key_exists('fromAddress', $options)) {
            throw new FromDetailsRequiredException('fromAddress');
        }

        return true;
    }

    /**
     * @param MimeMessage $body
     * @param array       $options
     *
     * @return void
     */
    private function populateBody(MimeMessage $body, array $options): void
    {
        $html = new MimePart($this->self->getHtmlBody($options['scriptsPath'], $options['layoutsPath']));
        $html->setType('text/html');
        $body->addPart($html);
    }

    /**
     * @return Message
     */
    private function createMessage(): Message
    {
        $email = new Message();
        $email->setEncoding('UTF-8');
        $email->setSubject($this->self->getSubject());
        $email->addTo($this->self->getToAddress());

        return $email;
    }

    /**
     * @param Message $email
     * @param array   $options
     *
     * @return void
     */
    private function setFromAndReplyTo(Message $email, array $options): void
    {
        //If a from address is not specified, grab the default from address/name.
        $fromName = '';

        if (\array_key_exists('fromName', $options) && !($fromName = $this->self->getFromName())) {
            $fromName = $options['fromName'];
        }

        //We always send from our authorized from address - replyTo is sent based on the messages from address
        $fromAddress = $options['fromAddress'];

        $email->setFrom($fromAddress, $fromName);

        if ($replyTo = $this->self->getFromAddress()) {
            $email->setReplyTo($replyTo, $fromName);
        }
    }

    /**
     * @param MimeMessage $body
     *
     * @return void
     */
    private function setAttachments(MimeMessage $body): void
    {
        foreach ($this->self->getAttachments() as $attachment) {
            $this->validateAttachment($attachment);

            $body->addPart($this->getMimePartFromAttachment($attachment));
        }
    }

    /**
     * @param Message $email
     * @param array   $options
     *
     * @return void
     */
    private function populateHeaders(Message $email, array $options): void
    {
        $headers = $email->getHeaders();

        foreach ($options as $header => $option) {
            if (false !== \strpos($header, 'X-MC')) {
                if (\is_bool($option)) {
                    $option = $option ? 'true' : 'false';
                }

                $headers->addHeaderLine($header, $option);
            }
        }

        $headers->addHeaderLine('X-MC-Subaccount', $this->subaccount);

        $email->setHeaders($headers);
    }

    /**
     * @param AttachmentProvider $attachment
     *
     * @return void
     * @throws AttachmentNotFoundException
     */
    private function validateAttachment(AttachmentProvider $attachment): void
    {
        if (!\file_exists($attachment->getFilePath()) && !\is_readable($attachment->getFilePath())) {
            throw new AttachmentNotFoundException(
                \sprintf(
                    'The file %s was not able to be located by the notification attachment system',
                    $attachment->getFilePath()
                )
            );
        }
    }

    /**
     * @param AttachmentProvider $attachment
     *
     * @return MimePart
     */
    private function getMimePartFromAttachment(AttachmentProvider $attachment): MimePart
    {
        $part = new MimePart(\fopen($attachment->getFilePath(), 'rb'));

        $part->setType($attachment->getMimeType())
             ->setFileName($attachment->getFileName())
             ->setEncoding(Mime::ENCODING_BASE64)
             ->setDisposition(Mime::DISPOSITION_ATTACHMENT);

        return $part;
    }
}
