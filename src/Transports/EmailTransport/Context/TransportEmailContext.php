<?php

namespace ServiceCore\Notification\Transports\EmailTransport\Context;

use Laminas\Mail\Transport\TransportInterface;
use ServiceCore\Notification\Transports\Core\Role\Transporter;
use ServiceCore\Notification\Transports\EmailTransport\Role\EmailTransporter;
use ServiceCore\Notification\Transports\EmailTransport\RoleData\EmailTransporterData;

class TransportEmailContext implements Transporter
{
    public const FLAG = 0b010;

    /** @var EmailTransporter */
    private $email;

    /**
     * @param EmailTransporterData $emailData
     * @param TransportInterface   $mailTransport
     * @param string               $subaccount
     */
    public function __construct(
        EmailTransporterData $emailData,
        TransportInterface $mailTransport,
        string $subaccount
    ) {
        $this->email = new EmailTransporter($emailData, $mailTransport, $subaccount);
    }

    /**
     * @param array $options
     *
     * @return bool|null
     */
    public function transport(array $options = []): ?bool
    {
        return $this->email->transport($options);
    }
}
