<?php

namespace ServiceCore\Notification\Transports\EmailTransport\Exception;

use Exception;

class AttachmentNotFoundException extends Exception
{
}
