<?php

namespace ServiceCore\Notification\Transports\EmailTransport\Exception;

use Exception;

class FromDetailsRequiredException extends Exception
{
    /**
     * @param string $missingDetail
     */
    public function __construct(string $missingDetail)
    {
        parent::__construct(
            \sprintf(
                'From detail %s missing in from configuration array',
                $missingDetail
            )
        );
    }
}
