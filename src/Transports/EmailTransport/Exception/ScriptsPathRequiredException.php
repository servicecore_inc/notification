<?php

namespace ServiceCore\Notification\Transports\EmailTransport\Exception;

use Exception;

class ScriptsPathRequiredException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'The required parameter "scriptsPath" is missing from the options array for the email transport'
        );
    }
}
