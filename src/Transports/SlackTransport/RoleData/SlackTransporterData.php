<?php

namespace ServiceCore\Notification\Transports\SlackTransport\RoleData;

interface SlackTransporterData
{
    public function getTo(): string;

    public function getFrom(): string;

    public function getIcon(): ?string;

    public function getMessage(): string;

    public function getAttachmentPaths(): array;
}
