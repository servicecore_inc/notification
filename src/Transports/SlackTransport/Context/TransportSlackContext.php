<?php

namespace ServiceCore\Notification\Transports\SlackTransport\Context;

use Maknz\Slack\Attachment;
use Maknz\Slack\Client;
use ServiceCore\Notification\Transports\Core\Role\Transporter;
use ServiceCore\Notification\Transports\SlackTransport\RoleData\SlackTransporterData;

class TransportSlackContext implements Transporter
{
    public const TYPE = 'slack';
    public const FLAG = 0b100;

    private Client $slack;

    private SlackTransporterData $slackData;

    public function __construct(SlackTransporterData $slackData, Client $slack)
    {
        $this->slack     = $slack;
        $this->slackData = $slackData;
    }

    public function transport(array $options = []): void
    {
        $message = $this->slack->createMessage();

        $message->enableMarkdown();

        $message->to($this->slackData->getTo())
                ->from($this->slackData->getFrom())
                ->withIcon($this->slackData->getIcon())
                ->setText($this->slackData->getMessage());

        if (\count($this->slackData->getAttachmentPaths()) > 0) {
            foreach ($this->slackData->getAttachmentPaths() as $attachmentPath) {
                if (\file_exists($attachmentPath)) {
                    $attachment = new Attachment(['text' => \file_get_contents($attachmentPath)]);

                    $message->attach($attachment);
                }
            }
        }

        $this->slack->send($message);
    }
}
