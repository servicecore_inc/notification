<?php

namespace ServiceCore\Notification\Transports\SlackTransport\Exception;

class MissingParameterException extends \Exception
{
    /**
     * @param string $missing
     * @param int    $from
     */
    public function __construct($missing, $from)
    {
        parent::__construct(
            \sprintf(
                'The required parameter %s was missing from the %s configuration',
                $missing,
                $from
            )
        );
    }
}
